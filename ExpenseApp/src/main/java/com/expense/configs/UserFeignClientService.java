package com.expense.configs;

import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name="user", url="${user.base.url}")
public interface UserFeignClientService {

}
