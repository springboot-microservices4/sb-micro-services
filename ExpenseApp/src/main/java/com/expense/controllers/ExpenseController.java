package com.expense.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.expense.models.BalanceStatementModel;
import com.expense.models.CardDetailsModel;
import com.expense.models.CategoriesModel;
import com.expense.models.CommonResponse;
import com.expense.models.PaymentTypeModel;
import com.expense.models.UserStatementModel;
import com.expense.services.ExpenseService;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class ExpenseController {
	
	@Autowired
	private ExpenseService expenseService;

	@GetMapping("/v1/get/balance-statement/{userId}")
	@CircuitBreaker(name="expense", fallbackMethod = "fallbackMethod")
	public  CommonResponse<UserStatementModel> getBalanceStatement(@PathVariable("userId") Integer userId) {
		return expenseService.getBalanceStatement(userId);
	}
	
	@PostMapping("/v1/post/save-transaction")
	public CommonResponse<Boolean> saveTransaction(@RequestBody BalanceStatementModel bsModel) {
		return expenseService.saveTransaction(bsModel);
	}
	
	public CommonResponse<UserStatementModel> fallbackMethod(Integer userId, RuntimeException runtimeException) {
		CommonResponse<UserStatementModel> res = new CommonResponse<UserStatementModel>();
		res.setMessage("Apolozise!!, Currently we are unable provide balance statement");
		res.setStatusCode(500);
		res.setResponse(null);
		return res;
	}
	
	
	@PostMapping("/v1/post/add-category")
	public CommonResponse<Boolean> addCategory(@RequestBody CategoriesModel model) {
		return expenseService.addCategory(model);
	}
	
	@PostMapping("/v1/post/add-payment-type")
	public CommonResponse<Boolean> addPaymentType(@RequestBody PaymentTypeModel model) {
		return expenseService.addPaymentType(model);
	}
	
	@GetMapping("/v1/get/categories/{userId}")
	public  CommonResponse<List<CategoriesModel>> getCategories(@PathVariable("userId") Integer userId) {
		return expenseService.getCategories(userId);
	}
	
	@GetMapping("/v1/get/payment-types/{userId}")
	public  CommonResponse<List<PaymentTypeModel>> getPaymentTypes(@PathVariable("userId") Integer userId) {
		return expenseService.getPaymentTypes(userId);
	}
	
	@PostMapping("/v1/post/add-card-details")
	public CommonResponse<Boolean> addCardDetails(@RequestBody CardDetailsModel model) {
		return expenseService.addCardDetails(model);
	}
	
	@GetMapping("/v1/get/card-details/{userId}")
	public  CommonResponse<List<CardDetailsModel>> getCardDetails(@PathVariable("userId") Integer userId) {
		return expenseService.getCardDetails(userId);
	}
}
