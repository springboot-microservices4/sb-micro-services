package com.expense.entities;

import java.io.Serializable;
import java.time.LocalDateTime;

import org.springframework.boot.autoconfigure.amqp.RabbitConnectionDetails.Address;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Data;

/**
 * @author Manikanta
 * @since 12/08/2023
 */
@Entity
@Data
@Table(name = "balance_statement")
public class BalanceStatementDet implements Serializable {

	private static final long serialVersionUID = -3117074356782211641L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "statement_id")
	private int statementId;

	@Column(name = "inward_amt", length = 15)
	private Double inwardAmt;

	@Column(name = "outward_amt", length = 15)
	private Double outwardAmt;

	@Column(name = "balance", length = 15)
	private Double balance;

	@Column(name = "amt_type", length = 15)
	private String amtType;

	@Column(name = "description")
	private String description;
	
	@Column(name = "transaction_date")
	private LocalDateTime transactionDate;

	@Column(name = "created_date")
	private LocalDateTime createdDate;

	@Column(name = "modified_date")
	private LocalDateTime modifiedDate;

	@Column(name = "user_id")
	private int userId;

	@ManyToOne
	@JoinColumn(name = "category_id", nullable = false)
	private Categories categories;

	@ManyToOne
	@JoinColumn(name = "payment_type_id", nullable = false)
	private PaymentTypes paymentTypes;

	@ManyToOne
	@JoinColumn(name = "card_id")
	private CardDetails cardDetails;

	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "credTransaction", referencedColumnName = "card_transaction_id")
	private CreditCardTransactions credTransaction;

}
