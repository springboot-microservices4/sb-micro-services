package com.expense.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Data;

/**
 * @author Manikanta
 * @since 12/08/2023
 */
@Entity
@Data
@Table(name = "categories")
public class Categories implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4150096875075866640L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="category_id")
	private int categoryId;
	
	@Column(name="category_name", length = 200)
	private String categoryName;
	
	@Column(name="category_value", length = 200)
	private String categoryValue;
	
	@Column(name="created_date")
	private LocalDateTime createdDate;
	
	@Column(name="modified_date")
	private LocalDateTime modifiedDate;
	
	@Column(name="user_id")
	private int userId;
	
	@OneToMany(mappedBy="categories")
    private List<BalanceStatementDet> transactions;
	
}
