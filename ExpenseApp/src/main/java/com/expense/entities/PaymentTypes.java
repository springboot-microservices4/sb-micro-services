package com.expense.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Data;

/**
 * @author Manikanta
 * @since 12/08/2023
 */
@Entity
@Data
@Table(name = "payment_types")
public class PaymentTypes  implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -7327707059302637422L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="payment_type_id")
	private int paymentTypeId;
	
	@Column(name="payment_type", length = 200)
	private String paymentType;
	
	@Column(name="payment_type_value", length = 200)
	private String paymentTypeValue;
	
	@Column(name="created_date")
	private LocalDateTime createdDate;
	
	@Column(name="modified_date")
	private LocalDateTime modifiedDate;
	
	@Column(name="user_id")
	private int userId;
	
	@OneToMany(mappedBy="paymentTypes")
    private List<BalanceStatementDet> transactions;
}