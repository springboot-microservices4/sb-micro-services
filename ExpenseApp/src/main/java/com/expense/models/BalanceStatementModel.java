package com.expense.models;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BalanceStatementModel {

	private int statementId;
	private Double inwardAmt;
	private Double outwardAmt;
	private Double balance;
	private String amtType;
	private String createdDate;
	private String modifiedDate;
	private String description;
	private String transactionDate;
	private String cardTransactionType;
	private CategoriesModel category;
	private CardDetailsModel cardDetails;
	private PaymentTypeModel paymentType;
	private int userId;

}
