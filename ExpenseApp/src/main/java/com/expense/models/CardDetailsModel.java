package com.expense.models;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.Data;

@Data
public class CardDetailsModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6452953488050962091L;

	private int cardId;

	private String cardName;

	private String cardNumber;

	private String bankName;

	private Double cardLimit;

	private LocalDateTime createdDate;

	private LocalDateTime modifiedDate;

	private int userId;
}
