package com.expense.models;

import java.io.Serializable;

import lombok.Data;

@Data
public class CategoriesModel implements Serializable {

	private static final long serialVersionUID = -3117074356782211641L;

	private int categoryId;

	private String categoryName;
	
	private String categoryValue;

	private int userId;

}
