package com.expense.models;

import java.io.Serializable;
import java.time.LocalDateTime;

import jakarta.persistence.Column;
import lombok.Data;

@Data
public class CreditCardTransactions implements Serializable {

	/**
		 * 
		 */
	private static final long serialVersionUID = -8888082099123003141L;

	private int cardTransactionId;

	private Double inwardAmt;

	private Double outwardAmt;

	private Double balance;

	private String amtType;

	private String description;

	private LocalDateTime createdDate;

	private LocalDateTime modifiedDate;

	@Column(name = "user_id")
	private int userId;
}
