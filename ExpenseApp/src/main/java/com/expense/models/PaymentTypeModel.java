package com.expense.models;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.Data;

@Data
public class PaymentTypeModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 228208084759422508L;

	private int paymentTypeId;

	private String paymentType;

	private String paymentTypeValue;
	
	private LocalDateTime createdDate;

	private LocalDateTime modifiedDate;

	private int userId;

}
