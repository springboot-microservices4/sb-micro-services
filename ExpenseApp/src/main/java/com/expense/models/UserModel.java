package com.expense.models;

import lombok.Data;

@Data
public class UserModel {

private int userId;
	
	private String firstName;
	
	private String lastName;
	
	private String userEmail;
	
	private String mobileNo;
	
	private String isDeleted;//1: not deleted, 0: deleted
	
	private String createdDate;
	
	private String modifiedDate;
	
	private UserStatementModel userStatement;
}
