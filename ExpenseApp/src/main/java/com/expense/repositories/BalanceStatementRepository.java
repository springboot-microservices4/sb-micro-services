package com.expense.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.expense.entities.BalanceStatementDet;

public interface BalanceStatementRepository extends JpaRepository<BalanceStatementDet, Integer>{


	List<BalanceStatementDet> findByUserIdOrderByModifiedDateDesc(Integer id);

	@Query("from BalanceStatementDet bs WHERE bs.userId = ?1 order by bs.statementId ASC")
	BalanceStatementDet getLatestRecord(int userId);

	BalanceStatementDet findTopByUserIdOrderByStatementIdAsc(int userId);

	BalanceStatementDet findTopByUserIdOrderByStatementIdDesc(int userId);

}
