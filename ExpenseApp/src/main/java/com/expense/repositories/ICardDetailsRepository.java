package com.expense.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.expense.entities.CardDetails;

public interface ICardDetailsRepository extends JpaRepository<CardDetails, Integer>{

	CardDetails findByUserId(int userId);

	List<CardDetails> findByUserIdOrderByModifiedDateDesc(Integer userId);

	CardDetails findByCardId(int cardId);

}
