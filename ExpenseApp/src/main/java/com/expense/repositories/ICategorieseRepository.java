package com.expense.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.expense.entities.Categories;

public interface ICategorieseRepository extends JpaRepository<Categories, Integer>{

	Categories findByUserId(int userId);

	List<Categories> findByUserIdOrderByModifiedDateDesc(Integer userId);

	Categories findByCategoryId(int categoryId);

}
