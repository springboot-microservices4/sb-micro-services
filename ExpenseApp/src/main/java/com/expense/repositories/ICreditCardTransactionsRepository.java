package com.expense.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.expense.entities.CardDetails;
import com.expense.entities.CreditCardTransactions;

public interface ICreditCardTransactionsRepository extends JpaRepository<CreditCardTransactions, Integer>{

	CreditCardTransactions findTopByCardDetailsOrderByCardTransactionIdDesc(CardDetails cardDetails);

}
