package com.expense.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.expense.entities.PaymentTypes;

public interface IPaymentTypeRepository extends JpaRepository<PaymentTypes, Integer>{

	PaymentTypes findByUserId(int userId);

	List<PaymentTypes> findByUserIdOrderByModifiedDateDesc(Integer userId);

	PaymentTypes findByPaymentTypeId(int paymentTypeId);

}