package com.expense.services;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.expense.entities.BalanceStatementDet;
import com.expense.entities.CardDetails;
import com.expense.entities.Categories;
import com.expense.entities.CreditCardTransactions;
import com.expense.entities.PaymentTypes;
import com.expense.models.BalanceStatementModel;
import com.expense.models.CardDetailsModel;
import com.expense.models.CategoriesModel;
import com.expense.models.CommonResponse;
import com.expense.models.PaymentTypeModel;
import com.expense.models.UserStatementModel;
import com.expense.repositories.BalanceStatementRepository;
import com.expense.repositories.ICardDetailsRepository;
import com.expense.repositories.ICategorieseRepository;
import com.expense.repositories.ICreditCardTransactionsRepository;
import com.expense.repositories.IPaymentTypeRepository;
import com.expense.utilities.ExpenseConstants;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ExpenseService {
	@Autowired
	private BalanceStatementRepository bsRepo;
	@Autowired
	private ICategorieseRepository categoriesRepo;
	@Autowired
	private IPaymentTypeRepository paymentTypesRepo;
	@Autowired
	private ICardDetailsRepository cardDetailsRepo;
	@Autowired
	private ICreditCardTransactionsRepository creditCardTransactionsRepo;
	
	DateTimeFormatter fomatter = DateTimeFormatter.ofPattern(ExpenseConstants.DATE_FORMAT);

	@Transactional
	public CommonResponse<Boolean> saveTransaction(BalanceStatementModel bsModel) {
		CommonResponse<Boolean> response = new CommonResponse<Boolean>();
		try {
			BalanceStatementDet bsDet = null;
			if(bsModel != null && bsModel.getAmtType() != null) {
				Categories category = null;
				PaymentTypes paymentType = null;
				CardDetails cardDetails = null;
				if(bsModel.getCategory() != null && bsModel.getCategory().getCategoryId() > 0) {
					category = categoriesRepo.findByCategoryId(bsModel.getCategory().getCategoryId());
				}
				
				if(bsModel.getPaymentType() != null && bsModel.getPaymentType().getPaymentTypeId() > 0) {
					paymentType = paymentTypesRepo.findByPaymentTypeId(bsModel.getPaymentType().getPaymentTypeId());
				}
				
				if(bsModel.getCardDetails() != null && bsModel.getCardDetails().getCardId() > 0) {
					cardDetails = cardDetailsRepo.findByCardId(bsModel.getCardDetails().getCardId());
				}
				
				
				
				Double balance = 0.0;
				bsDet = bsRepo.findTopByUserIdOrderByStatementIdDesc(bsModel.getUserId());
				if(bsDet != null) {
					balance = bsDet.getBalance();
					bsDet = new BalanceStatementDet();
				} else {
					bsDet = new BalanceStatementDet();
				}
				
				if(paymentType.getPaymentTypeValue().equals("CREDIT_CARD")) {
					if(bsModel.getCardTransactionType().equals("SHOPPING")){
						bsDet.setAmtType(ExpenseConstants.CREDIT);
						bsDet.setBalance(balance + bsModel.getOutwardAmt());
						bsDet.setCreatedDate(LocalDateTime.now());
						bsDet.setModifiedDate(LocalDateTime.now());
						bsDet.setInwardAmt(bsModel.getOutwardAmt());
						bsDet.setOutwardAmt(0.0);
						bsDet.setTransactionDate(ExpenseConstants.convertStringToLocalDateTime(bsModel.getTransactionDate(), ExpenseConstants.DATE_FORMAT));
						bsDet.setDescription("Credited From "+ cardDetails.getCardName());
						bsDet.setCategories(category);
						bsDet.setPaymentTypes(paymentType);
						bsDet.setUserId(bsModel.getUserId());
						bsDet.setCardDetails(cardDetails);
//						bsDet.setCredTransaction(saveCreditCardTransaction);
						bsRepo.save(bsDet);
						
						balance = bsDet.getBalance();
						bsDet = new BalanceStatementDet();
						bsDet.setAmtType(ExpenseConstants.DEBIT);
						bsDet.setBalance(balance - bsModel.getOutwardAmt());
						bsDet.setCreatedDate(LocalDateTime.now());
						bsDet.setModifiedDate(LocalDateTime.now());
						bsDet.setInwardAmt(0.0);
						bsDet.setTransactionDate(ExpenseConstants.convertStringToLocalDateTime(bsModel.getTransactionDate(), ExpenseConstants.DATE_FORMAT));
						bsDet.setOutwardAmt(bsModel.getOutwardAmt());
						bsDet.setDescription(bsModel.getDescription());
						bsDet.setCategories(category);
						bsDet.setPaymentTypes(paymentType);
						bsDet.setCardDetails(cardDetails);
						bsDet.setUserId(bsModel.getUserId());
						bsRepo.save(bsDet);
						CreditCardTransactions saveCreditCardTransaction = saveCreditCardTransaction(bsModel, cardDetails, bsDet);
						bsDet.setCredTransaction(saveCreditCardTransaction);
						bsRepo.save(bsDet);
					} else if(bsModel.getCardTransactionType().equals("WITHDRAWL")){
						bsDet.setAmtType(ExpenseConstants.CREDIT);
						bsDet.setBalance(balance + bsModel.getInwardAmt());
						bsDet.setCreatedDate(LocalDateTime.now());
						bsDet.setModifiedDate(LocalDateTime.now());
						bsDet.setInwardAmt(bsModel.getInwardAmt());
						bsDet.setOutwardAmt(0.0);
						bsDet.setTransactionDate(ExpenseConstants.convertStringToLocalDateTime(bsModel.getTransactionDate(), ExpenseConstants.DATE_FORMAT));
						bsDet.setDescription("Credited From "+ cardDetails.getCardName());
						bsDet.setCategories(category);
						bsDet.setPaymentTypes(paymentType);
						bsDet.setUserId(bsModel.getUserId());
						bsDet.setCardDetails(cardDetails);
						bsRepo.save(bsDet);
						CreditCardTransactions saveCreditCardTransaction = saveCreditCardTransaction(bsModel, cardDetails, bsDet);
						bsDet.setCredTransaction(saveCreditCardTransaction);
						bsRepo.save(bsDet);
					} else {
						bsDet.setAmtType(ExpenseConstants.DEBIT);
						bsDet.setBalance(balance - bsModel.getOutwardAmt());
						bsDet.setCreatedDate(LocalDateTime.now());
						bsDet.setModifiedDate(LocalDateTime.now());
						bsDet.setInwardAmt(0.0);
						bsDet.setTransactionDate(ExpenseConstants.convertStringToLocalDateTime(bsModel.getTransactionDate(), ExpenseConstants.DATE_FORMAT));
						bsDet.setOutwardAmt(bsModel.getOutwardAmt());
						bsDet.setDescription(bsModel.getDescription());
						bsDet.setCategories(category);
						bsDet.setPaymentTypes(paymentType);
						bsDet.setCardDetails(cardDetails);
						bsDet.setUserId(bsModel.getUserId());
						bsRepo.save(bsDet);
						
						CreditCardTransactions saveCreditCardTransaction = saveCreditCardTransaction(bsModel, cardDetails, bsDet);
						bsDet.setCredTransaction(saveCreditCardTransaction);
						bsRepo.save(bsDet);
					}
				} else {
					if(bsModel.getAmtType().equals(ExpenseConstants.CREDIT)) {
						bsDet.setAmtType(ExpenseConstants.CREDIT);
						bsDet.setBalance(balance + bsModel.getInwardAmt());
						bsDet.setCreatedDate(LocalDateTime.now());
						bsDet.setModifiedDate(LocalDateTime.now());
						bsDet.setInwardAmt(bsModel.getInwardAmt());
						bsDet.setOutwardAmt(0.0);
						bsDet.setTransactionDate(ExpenseConstants.convertStringToLocalDateTime(bsModel.getTransactionDate(), ExpenseConstants.DATE_FORMAT));
						bsDet.setDescription(bsModel.getDescription());
						bsDet.setCategories(category);
						bsDet.setPaymentTypes(paymentType);
						bsDet.setUserId(bsModel.getUserId());
						bsRepo.save(bsDet);
					}
					if(bsModel.getAmtType().equals(ExpenseConstants.DEBIT)) {
						bsDet.setAmtType(ExpenseConstants.DEBIT);
						bsDet.setBalance(balance - bsModel.getOutwardAmt());
						bsDet.setCreatedDate(LocalDateTime.now());
						bsDet.setModifiedDate(LocalDateTime.now());
						bsDet.setInwardAmt(0.0);
						bsDet.setTransactionDate(ExpenseConstants.convertStringToLocalDateTime(bsModel.getTransactionDate(), ExpenseConstants.DATE_FORMAT));
						bsDet.setOutwardAmt(bsModel.getOutwardAmt());
						bsDet.setDescription(bsModel.getDescription());
						bsDet.setCategories(category);
						bsDet.setPaymentTypes(paymentType);
						bsDet.setUserId(bsModel.getUserId());
						bsRepo.save(bsDet);
					}
				}
				
				
				response.setMessage("Transaction Saved Successfully");
				response.setResponse(true);
				response.setStatusCode(200);
				log.info("Transaction saved successfully");
			}
		} catch (Exception ex) {
			response.setMessage("Transaction Saving Failed "+ex.getMessage());
			response.setResponse(false);
			response.setStatusCode(500);
		}
		return response;
	}
	
	@Transactional
	public CreditCardTransactions saveCreditCardTransaction(BalanceStatementModel bsModel, CardDetails cardDetails, BalanceStatementDet bsDet) {
		
		CreditCardTransactions credTransaction = null;
		double balance = 0.0;
		credTransaction = creditCardTransactionsRepo.findTopByCardDetailsOrderByCardTransactionIdDesc(cardDetails);
		
		if(credTransaction != null) {
			balance = credTransaction.getBalance();
			credTransaction = new CreditCardTransactions();
		} else {
			credTransaction = new CreditCardTransactions();
		}
		
		if(bsModel.getCardTransactionType().equals("BILL_PAY")){
			credTransaction.setAmtType(ExpenseConstants.CREDIT);
			credTransaction.setBalance(balance + bsModel.getOutwardAmt());
			credTransaction.setCreatedDate(LocalDateTime.now());
			credTransaction.setModifiedDate(LocalDateTime.now());
			credTransaction.setInwardAmt(bsModel.getInwardAmt());
			credTransaction.setOutwardAmt(0.0);
			credTransaction.setTransactionDate(ExpenseConstants.convertStringToLocalDateTime(bsModel.getTransactionDate(), ExpenseConstants.DATE_FORMAT));
			credTransaction.setDescription(bsModel.getDescription());
			credTransaction.setUserId(bsModel.getUserId());
			credTransaction.setCardDetails(cardDetails);
			credTransaction.setTransaction(bsDet);		
			creditCardTransactionsRepo.save(credTransaction);
		} else if(bsModel.getCardTransactionType().equals("EMI")){
			credTransaction.setAmtType(ExpenseConstants.DEBIT);
			credTransaction.setBalance(balance - bsModel.getOutwardAmt());
			credTransaction.setCreatedDate(LocalDateTime.now());
			credTransaction.setModifiedDate(LocalDateTime.now());
			credTransaction.setInwardAmt(0.0);
			credTransaction.setTransactionDate(ExpenseConstants.convertStringToLocalDateTime(bsModel.getTransactionDate(), ExpenseConstants.DATE_FORMAT));
			credTransaction.setOutwardAmt(bsModel.getOutwardAmt());
			credTransaction.setDescription("EMI: "+bsModel.getDescription());
			credTransaction.setUserId(bsModel.getUserId());
			credTransaction.setCardDetails(cardDetails);
			credTransaction.setTransaction(bsDet);
			creditCardTransactionsRepo.save(credTransaction);
			
			balance = credTransaction.getBalance();
			
			credTransaction = new CreditCardTransactions();
			credTransaction.setAmtType(ExpenseConstants.CREDIT);
			credTransaction.setBalance(balance + bsModel.getOutwardAmt());
			credTransaction.setCreatedDate(LocalDateTime.now());
			credTransaction.setModifiedDate(LocalDateTime.now());
			credTransaction.setInwardAmt(bsModel.getInwardAmt());
			credTransaction.setOutwardAmt(0.0);
			credTransaction.setTransactionDate(ExpenseConstants.convertStringToLocalDateTime(bsModel.getTransactionDate(), ExpenseConstants.DATE_FORMAT));
			credTransaction.setDescription("EMI: "+bsModel.getDescription());
			credTransaction.setUserId(bsModel.getUserId());
			credTransaction.setCardDetails(cardDetails);
			credTransaction.setTransaction(bsDet);		
			creditCardTransactionsRepo.save(credTransaction);
		} else {
			credTransaction.setAmtType(ExpenseConstants.DEBIT);
			credTransaction.setBalance(balance - bsModel.getOutwardAmt());
			credTransaction.setCreatedDate(LocalDateTime.now());
			credTransaction.setModifiedDate(LocalDateTime.now());
			credTransaction.setInwardAmt(0.0);
			credTransaction.setTransactionDate(ExpenseConstants.convertStringToLocalDateTime(bsModel.getTransactionDate(), ExpenseConstants.DATE_FORMAT));
//			log.info("Outward amount: "+bsModel.getInwardAmt()+"");
			credTransaction.setOutwardAmt(bsModel.getOutwardAmt());
			log.info("Outward amount: "+credTransaction.getInwardAmt()+"");
			credTransaction.setDescription(bsModel.getDescription());
			credTransaction.setUserId(bsModel.getUserId());
			credTransaction.setCardDetails(cardDetails);
			credTransaction.setTransaction(bsDet);
			creditCardTransactionsRepo.save(credTransaction);
		}
		
		return credTransaction;
	}

	public CommonResponse<UserStatementModel> getBalanceStatement(Integer id) {
		Double totalBalance = 0.0;
		Double tatalInwards = 0.0;
		Double totalOutWards = 0.0;
		
		CommonResponse<UserStatementModel> response = new CommonResponse<UserStatementModel>();
		UserStatementModel statement = new UserStatementModel(); 
		List<BalanceStatementModel> bsModelList = new ArrayList<BalanceStatementModel>();
		try {
			BalanceStatementDet latestRecord = bsRepo.findTopByUserIdOrderByStatementIdDesc(id);
			if(latestRecord != null) {
				totalBalance = latestRecord.getBalance();
			}
			List<BalanceStatementDet> statementModelList = bsRepo.findByUserIdOrderByModifiedDateDesc(id);
			if(statementModelList != null) {
				
				for(BalanceStatementDet stmt: statementModelList) {
					BalanceStatementModel bsModel = new BalanceStatementModel();
					tatalInwards = tatalInwards + stmt.getInwardAmt();
					totalOutWards = totalOutWards + stmt.getOutwardAmt();
					BeanUtils.copyProperties(stmt, bsModel);
					bsModel.setCreatedDate(stmt.getCreatedDate().format(fomatter));
					bsModel.setModifiedDate(stmt.getModifiedDate().format(fomatter));
					CategoriesModel category = new CategoriesModel();
					PaymentTypeModel paymnetType = new PaymentTypeModel();
					CardDetailsModel cardDetails = new CardDetailsModel();
					if(stmt.getCategories() != null) {
						BeanUtils.copyProperties(stmt.getCategories(), category);
					}
					if(stmt.getPaymentTypes() != null) {
						BeanUtils.copyProperties(stmt.getPaymentTypes(), paymnetType);
					}
					if(stmt.getCardDetails() != null) {
						BeanUtils.copyProperties(stmt.getCardDetails(), cardDetails);
					}
					bsModel.setTransactionDate(stmt.getTransactionDate().format(fomatter));
					bsModel.setCategory(category);
					bsModel.setPaymentType(paymnetType);
					bsModel.setCardDetails(cardDetails);
					bsModelList.add(bsModel);
				}
				
				statement.setBsModel(bsModelList);
				statement.setFinalBalance(totalBalance);
				statement.setTotalInwards(tatalInwards);
				statement.setTotalOutWards(totalOutWards);
				statement.setUserId(id);
				
				response.setMessage("Getting Statement Successfully");
				response.setResponse(statement);
				response.setStatusCode(200);
				log.info("Getting Statement Successfully");
			}
		} catch(Exception ex) {
			response.setMessage("Fetch Failed "+ex.getMessage());
			response.setResponse(null);
			response.setStatusCode(500);
			throw new RuntimeException();
		}
		return response;
	}

	public CommonResponse<Boolean> addCategory(CategoriesModel model) {
		CommonResponse<Boolean> response = new CommonResponse<Boolean>();
		try {
			Categories category = null;
			if(model != null) {
				if(model.getUserId() > 1) {
					category = categoriesRepo.findByUserId(model.getUserId());
					category.setCategoryName(model.getCategoryName());
					category.setCategoryValue(model.getCategoryValue());
					category.setModifiedDate(LocalDateTime.now());
				} else {
					category = new Categories();
					category.setCategoryName(model.getCategoryName());
					category.setCategoryValue(model.getCategoryValue());
					category.setModifiedDate(LocalDateTime.now());
					category.setUserId(model.getUserId());
					category.setCreatedDate(LocalDateTime.now());
				}
				
				categoriesRepo.save(category);
				response.setMessage("Category Added");
				response.setResponse(true);
				response.setStatusCode(200);
				log.info("Category Added");
			}
		} catch (Exception ex) {
			response.setMessage("Category Adding Failed "+ex.getMessage());
			response.setResponse(false);
			response.setStatusCode(500);
		}
		return response;
	}

	public CommonResponse<Boolean> addPaymentType(PaymentTypeModel model) {
		CommonResponse<Boolean> response = new CommonResponse<Boolean>();
		try {
			PaymentTypes paymentType = null;
			if(model != null) {
				if(model.getUserId() > 1) {
					paymentType = paymentTypesRepo.findByUserId(model.getUserId());
					paymentType.setPaymentType(model.getPaymentType());
					paymentType.setPaymentTypeValue(model.getPaymentTypeValue());
					paymentType.setModifiedDate(LocalDateTime.now());
				} else {
					paymentType = new PaymentTypes();
					paymentType.setPaymentTypeValue(model.getPaymentTypeValue());
					paymentType.setPaymentType(model.getPaymentType());
					paymentType.setModifiedDate(LocalDateTime.now());
					paymentType.setUserId(model.getUserId());
					paymentType.setCreatedDate(LocalDateTime.now());
				}
				
				paymentTypesRepo.save(paymentType);
				response.setMessage("Payment Type Added");
				response.setResponse(true);
				response.setStatusCode(200);
				log.info("Payment Type Added successfully");
			}
		} catch (Exception ex) {
			response.setMessage("Payment Type Addeding Failed "+ex.getMessage());
			response.setResponse(false);
			response.setStatusCode(500);
		}
		return response;
	}

	public CommonResponse<List<CategoriesModel>> getCategories(Integer userId) {
		CommonResponse<List<CategoriesModel>> response = new CommonResponse<List<CategoriesModel>>();
		try {
			List<CategoriesModel> categoriesList = new ArrayList<CategoriesModel>();
			List<Categories> categories = categoriesRepo.findByUserIdOrderByModifiedDateDesc(userId);
			for (Categories category : categories) {
				CategoriesModel categoryModel = new CategoriesModel();
				BeanUtils.copyProperties(category, categoryModel);
				categoriesList.add(categoryModel);
			}
			log.info("Categories Fetched");
			response.setMessage("Data Fetched");
			response.setResponse(categoriesList);
			response.setStatusCode(200);
		} catch (Exception e) {
			response.setMessage("Data Fetching Failed");
			response.setResponse(null);
			response.setStatusCode(500);
		}
		return response;
	}

	public CommonResponse<List<PaymentTypeModel>> getPaymentTypes(Integer userId) {
		CommonResponse<List<PaymentTypeModel>> response = new CommonResponse<List<PaymentTypeModel>>();
		try {
			List<PaymentTypeModel> paymentTypeList = new ArrayList<PaymentTypeModel>();
			List<PaymentTypes> paymentTypes = paymentTypesRepo.findByUserIdOrderByModifiedDateDesc(userId);
			for (PaymentTypes paymentType : paymentTypes) {
				PaymentTypeModel paymentTypeMModel = new PaymentTypeModel();
				BeanUtils.copyProperties(paymentType, paymentTypeMModel);
				paymentTypeList.add(paymentTypeMModel);
			}
			log.info("Payment TypesS Fetched");
			response.setMessage("Data Fetched");
			response.setResponse(paymentTypeList);
			response.setStatusCode(200);
		} catch (Exception e) {
			response.setMessage("Data Fetching Failed");
			response.setResponse(null);
			response.setStatusCode(500);
		}
		return response;
	}

	public CommonResponse<Boolean> addCardDetails(CardDetailsModel model) {
		CommonResponse<Boolean> response = new CommonResponse<Boolean>();
		try {
			CardDetails cardDetails = null;
			boolean isCredit = false;
			double newLimitAmount = 0.0;
			if(model != null) {
				
				if(model.getUserId() > 0) {
					Optional<CardDetails> cardDetails1 = cardDetailsRepo.findById(model.getCardId());
					if(cardDetails1.isPresent()) {
						cardDetails = cardDetails1.get();
						cardDetails.setBankName(model.getBankName());
						cardDetails.setCardLimit(model.getCardLimit());
						cardDetails.setCardName(model.getCardName());
						cardDetails.setCardNumber(model.getCardNumber());
						cardDetails.setModifiedDate(LocalDateTime.now());
						
						if(cardDetails.getCardLimit() > model.getCardLimit()) {
							newLimitAmount = cardDetails.getCardLimit() - model.getCardLimit();
							isCredit = false;
						} else {
							newLimitAmount = model.getCardLimit() - cardDetails.getCardLimit();
							isCredit = true;
						}
						
					} else {
						cardDetails = new CardDetails();
						cardDetails.setBankName(model.getBankName());
						cardDetails.setCardLimit(model.getCardLimit());
						cardDetails.setCardName(model.getCardName());
						cardDetails.setCardNumber(model.getCardNumber());
						cardDetails.setModifiedDate(LocalDateTime.now());
						cardDetails.setCreatedDate(LocalDateTime.now());
						cardDetails.setUserId(model.getUserId());
						isCredit = true;
						newLimitAmount = model.getCardLimit();
					}
					cardDetails = cardDetailsRepo.save(cardDetails);
					
					CreditCardTransactions credTransaction = null;
					Double balance = 0.0;
					credTransaction = creditCardTransactionsRepo.findTopByCardDetailsOrderByCardTransactionIdDesc(cardDetails);
					
					if(credTransaction != null) {
						balance = credTransaction.getBalance();
						credTransaction = new CreditCardTransactions();
					} else {
						credTransaction = new CreditCardTransactions();
					}
					
					if(isCredit){
						credTransaction.setAmtType(ExpenseConstants.CREDIT);
						credTransaction.setBalance(balance + newLimitAmount);
						credTransaction.setCreatedDate(LocalDateTime.now());
						credTransaction.setModifiedDate(LocalDateTime.now());
						credTransaction.setInwardAmt(newLimitAmount);
						credTransaction.setOutwardAmt(0.0);
						credTransaction.setTransactionDate(LocalDateTime.now());
						credTransaction.setDescription("Limit Increased");
						credTransaction.setUserId(model.getUserId());
						credTransaction.setCardDetails(cardDetails);
						creditCardTransactionsRepo.save(credTransaction);
					} else {
						credTransaction.setAmtType(ExpenseConstants.DEBIT);
						credTransaction.setBalance(balance - newLimitAmount);
						credTransaction.setCreatedDate(LocalDateTime.now());
						credTransaction.setModifiedDate(LocalDateTime.now());
						credTransaction.setInwardAmt(0.0);
						credTransaction.setTransactionDate(LocalDateTime.now());
						credTransaction.setOutwardAmt(newLimitAmount);
						credTransaction.setDescription("Limit Reduced");
						credTransaction.setUserId(model.getUserId());
						credTransaction.setCardDetails(cardDetails);
						creditCardTransactionsRepo.save(credTransaction);
					}
				}
				
				
				
				
				response.setMessage("Card Details Added");
				response.setResponse(true);
				response.setStatusCode(200);
				log.info("Card Details Added");
			}
		} catch (Exception ex) {
			response.setMessage("Card Details Addeding Failed "+ex.getMessage());
			response.setResponse(false);
			response.setStatusCode(500);
		}
		return response;
	}

	public CommonResponse<List<CardDetailsModel>> getCardDetails(Integer userId) {
		CommonResponse<List<CardDetailsModel>> response = new CommonResponse<List<CardDetailsModel>>();
		try {
			List<CardDetailsModel> paymentTypeList = new ArrayList<CardDetailsModel>();
			List<CardDetails> paymentTypes = cardDetailsRepo.findByUserIdOrderByModifiedDateDesc(userId);
			for (CardDetails paymentType : paymentTypes) {
				CardDetailsModel paymentTypeMModel = new CardDetailsModel();
				BeanUtils.copyProperties(paymentType, paymentTypeMModel);
				paymentTypeList.add(paymentTypeMModel);
			}
			log.info("Card Details Fetched");
			response.setMessage("Data Fetched");
			response.setResponse(paymentTypeList);
			response.setStatusCode(200);
		} catch (Exception e) {
			response.setMessage("Data Fetching Failed");
			response.setResponse(null);
			response.setStatusCode(500);
		}
		return response;
	}

}
