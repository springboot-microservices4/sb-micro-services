package com.expense.utilities;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class ExpenseConstants {

	public static final String DEBIT = "DEBIT";
	public static final String CREDIT = "CREDIT";
	public static final String TIME_FORMAT = "DD-MM-YYYY HH:mm";
	public static final String DATE_FORMAT = "dd MMM, yyyy";
	
	public static final LocalDateTime convertStringToLocalDateTime(String inputDate, String format) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
		LocalDate localDate = LocalDate.parse(inputDate, formatter);

        // Convert LocalDate to LocalDateTime (start of the day)
        LocalDateTime localDateTime = localDate.atStartOfDay();

        // Optional: Convert to ZonedDateTime if needed
        ZonedDateTime zonedDateTime = localDateTime.atZone(ZoneId.systemDefault());
		LocalDateTime date = localDateTime;
		return date;
	}
}
