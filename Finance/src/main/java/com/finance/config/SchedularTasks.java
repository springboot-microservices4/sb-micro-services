package com.finance.config;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.finance.entities.Finance;
import com.finance.repositories.IFinanceRepository;
import com.finance.utilities.FinanceConstants;
import com.finance.utilities.InterestCaclulatorUtil;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class SchedularTasks {

	@Autowired
	private IFinanceRepository financeRepo;

	@Scheduled(cron = "9 * * ? * *")
	public void calculatInterestEveryDay() {
		List<Finance> financeList = financeRepo.findAll();
		financeList.forEach(finance -> {
			int years = InterestCaclulatorUtil.getYears(finance.getLendOrBorrowDate(), LocalDateTime.now());
			int months = InterestCaclulatorUtil.getMonths(finance.getLendOrBorrowDate(), LocalDateTime.now());
			int days = InterestCaclulatorUtil.getDays(finance.getLendOrBorrowDate(), LocalDateTime.now());
			log.info(years + " years " + months + " months " + days + " days");

			double noOfHundreds = finance.getPrincipleAmt() / 100;
			double oneMonthInterest = noOfHundreds * finance.getInterestRate();
			double oneDayInterest = oneMonthInterest / 30;
			double yearsInterest = oneMonthInterest * (years * 12);
			double monthsInterest = oneMonthInterest * months;
			double daysInterest = oneDayInterest * days;
			log.info("One Month Interest: " + oneMonthInterest);
			log.info("Per day Interest: " + oneDayInterest);
			log.info("Years interest: " + yearsInterest);
			log.info("Months interest: " + monthsInterest);
			log.info("Days interest: " + daysInterest);
			double totalInterest = yearsInterest + monthsInterest + daysInterest;
			log.info("Total Interest: " + totalInterest);
			double totAmt = totalInterest + finance.getPrincipleAmt();
			log.info("Total Amount: " + totAmt);
			finance.setDays(days);
			finance.setMonths(months);
			finance.setYears(years);
			finance.setInterest(totalInterest);
			finance.setTotalAmount(totalInterest + finance.getPrincipleAmt());
			finance.setStatus(FinanceConstants.RUNNING);
			finance.setModifiedDate(LocalDateTime.now());
			financeRepo.save(finance);
		});
	}

	/*
	 * @Scheduled(cron = "0/10 * * ? * *") public void
	 * calculatInterestEvery10Seconds() { List<Finance> financeList =
	 * financeRepo.findAll(); financeList.forEach(finance ->{ int years =
	 * InterestCaclulatorUtil.getYears(finance.getLendOrBorrowDate(),LocalDateTime.
	 * now()); int months =
	 * InterestCaclulatorUtil.getMonths(finance.getLendOrBorrowDate(),LocalDateTime.
	 * now()); int days =
	 * InterestCaclulatorUtil.getDays(finance.getLendOrBorrowDate(),LocalDateTime.
	 * now()); log.info(years + " years "+ months + " months "+ days + " days");
	 * 
	 * double noOfHundreds = finance.getPrincipleAmt() / 100; double
	 * oneMonthInterest = noOfHundreds * finance.getInterestRate(); double
	 * oneDayInterest = oneMonthInterest / 30; double yearsInterest =
	 * oneMonthInterest * (years * 12); double monthsInterest = oneMonthInterest *
	 * months; double daysInterest = oneDayInterest * days;
	 * log.info("One Month Interest: "+oneMonthInterest);
	 * log.info("Per day Interest: "+oneDayInterest);
	 * log.info("Years interest: "+yearsInterest);
	 * log.info("Months interest: "+monthsInterest);
	 * log.info("Days interest: "+daysInterest); double totalInterest =
	 * yearsInterest + monthsInterest + daysInterest;
	 * log.info("Total Interest: "+totalInterest); double totAmt = totalInterest +
	 * finance.getPrincipleAmt(); log.info("Total Amount: "+ totAmt);
	 * finance.setDays(days); finance.setMonths(months); finance.setYears(years);
	 * finance.setInterest(totalInterest); finance.setTotalAmount(totalInterest +
	 * finance.getPrincipleAmt()); finance.setModifiedDate(LocalDateTime.now());
	 * financeRepo.save(finance); }); }
	 */

}
