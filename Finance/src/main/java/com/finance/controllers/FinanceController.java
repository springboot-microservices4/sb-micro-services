package com.finance.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.finance.models.CommonResponse;
import com.finance.models.FinanceLines;
import com.finance.models.FinanceModel;
import com.finance.services.FinanceService;


@RestController
public class FinanceController {

	@Autowired
	private FinanceService financeService;
	@GetMapping("/v1/get/finance-statement/{userId}")
	public  CommonResponse<FinanceModel> getBalanceStatement(@PathVariable("userId") Integer userId) {
		return financeService.getFinanceList(userId);
	}
	
	@PostMapping("/v1/post/save-finance")
	public CommonResponse<Boolean> saveTransaction(@RequestBody FinanceLines  financeModel) {
		return financeService.saveLendOrBorrow(financeModel);
	}
}
