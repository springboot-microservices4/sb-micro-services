package com.finance.entities;

import java.io.Serializable;
import java.time.LocalDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table(name = "finance")
public class Finance implements Serializable{

	private static final long serialVersionUID = -3117074356782211641L;
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="finance_id")
	private int financeId;
	
	@Column(name="principle_amt", length = 15)
	private Double principleAmt;
	
	@Column(name="interest_rate", length = 15)
	private Double interestRate;
	
	@Column(name="finance_type", length = 15)
	private String financeType;
	
	@Column(name="borrower_name", length = 200)
	private String borrowerName;
	
	@Column(name="lender_name", length = 200)
	private String lenderName;
	
	@Column(name="status", length = 200)
	private String status;
	
	@Column(name="lend_or_borrow_date")
	private LocalDateTime lendOrBorrowDate;
	
	@Column(name="lend_or_borrow_end_ate")
	private LocalDateTime lendOrBorrowEndDate;
	
	@Column(name="years", length = 15)
	private int years;
	
	@Column(name="months", length = 15)
	private int months;
	
	@Column(name="days", length = 15)
	private int days;
	
	@Column(name="totoal_amount", length = 15)
	private Double totalAmount;
	
	@Column(name="interest", length = 15)
	private Double interest;
	
	@Column(name="created_date")
	private LocalDateTime createdDate;
	
	@Column(name="modified_date")
	private LocalDateTime modifiedDate;
	
	@Column(name="user_id")
	private int userId;
	
}