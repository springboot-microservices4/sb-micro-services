package com.finance.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FinanceLines {
	private double principleAmt;
	private double interestRate;
	private String lendOrBorrowDate;
	private String financeType;
	private String borrowerName;
	private String lenderName;
	private String status;

	private int years;
	private int months;
	private int days;

	private double totalAmount;
	private double interest;

	private String createdDate;
	private String modifiedDate;
	private int userId;
}
