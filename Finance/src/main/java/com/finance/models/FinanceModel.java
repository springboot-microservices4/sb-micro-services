package com.finance.models;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FinanceModel {

	private double totalPriciple;
	private double totalInterest;
	private double totalAmount;
	private List<FinanceLines> financeLines;
	private int userId;
}
