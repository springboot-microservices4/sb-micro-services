package com.finance.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.finance.entities.Finance;

public interface IFinanceRepository extends JpaRepository<Finance, Integer>{

	List<Finance> findByUserId(Integer userId);

	List<Finance> findByUserIdOrderByModifiedDateDesc(Integer userId);

}
