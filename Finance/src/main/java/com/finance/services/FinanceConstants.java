package com.finance.services;

public class FinanceConstants {

	public static final String DEBIT = "DEBIT";
	public static final String CREDIT = "CREDIT";
	public static final String TIME_FORMAT = "DD-MM-YYYY HH:mm";
	public static final String DATE_FORMAT = "dd MMM, yyyy";
}
