package com.finance.services;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.finance.entities.Finance;
import com.finance.models.CommonResponse;
import com.finance.models.FinanceLines;
import com.finance.models.FinanceModel;
import com.finance.repositories.IFinanceRepository;
import com.finance.utilities.FinanceConstants;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class FinanceService {
	
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FinanceConstants.DATE_FORMAT);
	
	@Autowired
	private IFinanceRepository financeRepo;
	
	public CommonResponse<Boolean> saveLendOrBorrow(FinanceLines financeModel) {
		CommonResponse<Boolean> response = new CommonResponse<Boolean>();
		try {
			Finance finance = new Finance();
			
			finance.setCreatedDate(LocalDateTime.now());
			finance.setDays(0);
			finance.setFinanceType(financeModel.getFinanceType());
			finance.setInterest(0.0);
			finance.setInterestRate(financeModel.getInterestRate());
			finance.setLendOrBorrowDate(LocalDateTime.now());
			finance.setModifiedDate(LocalDateTime.now());
			finance.setMonths(0);
			finance.setPrincipleAmt(financeModel.getPrincipleAmt());
			finance.setTotalAmount(0.0);
			finance.setUserId(financeModel.getUserId());
			finance.setYears(0);
			finance.setBorrowerName(financeModel.getBorrowerName());
			finance.setLenderName(financeModel.getLenderName());
			finance.setStatus(FinanceConstants.STARTED);
			
			financeRepo.save(finance);
			log.info("Finance Saved succesfullly");
			response = new CommonResponse<Boolean>(200, "Finance created successfully", true);
		} catch (Exception e) {
			response = new CommonResponse<Boolean>(500, "Finance creattion failed", false);
		}
		return response;
	}

	public CommonResponse<FinanceModel> getFinanceList(Integer userId) {
		CommonResponse<FinanceModel> response = new CommonResponse<FinanceModel>();
		try {
			double totalPrinciple = 0.0;
			double totalInterest = 0.0;
			double totalAmount = 0.0;
			List<FinanceLines> financeList = new ArrayList<FinanceLines>();
			List<Finance> finances = financeRepo.findByUserIdOrderByModifiedDateDesc(userId);
			FinanceModel financeModel = new FinanceModel();
			for (Finance finance : finances) {
				FinanceLines line = new FinanceLines();
				line.setBorrowerName(finance.getBorrowerName());
				line.setCreatedDate(finance.getCreatedDate().format(formatter));
				line.setModifiedDate(finance.getModifiedDate().format(formatter));
				line.setDays(finance.getDays());
				line.setMonths(finance.getMonths());
				line.setYears(finance.getYears());
				line.setPrincipleAmt(finance.getPrincipleAmt());
				line.setInterest(finance.getInterest());
				line.setInterestRate(finance.getInterestRate());
				line.setTotalAmount(finance.getTotalAmount());
				line.setFinanceType(finance.getFinanceType());
				line.setLenderName(finance.getLenderName());
				line.setLendOrBorrowDate(finance.getLendOrBorrowDate().format(formatter));
				line.setStatus(finance.getStatus());
				line.setUserId(0);
				
				totalPrinciple = totalPrinciple + finance.getPrincipleAmt();
				totalInterest = totalInterest + finance.getInterest();
				totalAmount = totalAmount  + finance.getTotalAmount();
				
				financeList.add(line);
			}
			financeModel.setFinanceLines(financeList);
			financeModel.setTotalAmount(totalAmount);
			financeModel.setTotalInterest(totalInterest);
			financeModel.setTotalPriciple(totalPrinciple);
			financeModel.setUserId(userId);
			log.info("Finance Saved succesfullly");
			response.setMessage("Data Fetched");
			response.setResponse(financeModel);
			response.setStatusCode(200);
		} catch (Exception e) {
			response.setMessage("Data Fetched");
			response.setResponse(null);
			response.setStatusCode(500);
		}
		return response;
	}

}
