package com.finance.utilities;

public class FinanceConstants {

		public static final String LEND = "DEBIT";
		public static final String BORROW = "CREDIT";
		public static final String STARTED = "STARTED";
		public static final String RUNNING = "RUNNING";
		public static final String CLOSED = "CLOSED";
		
		public static final String DEBIT = "DEBIT";
		public static final String CREDIT = "CREDIT";
		public static final String TIME_FORMAT = "DD-MM-YYYY HH:mm";
		public static final String DATE_FORMAT = "dd MMM, yyyy";

}
