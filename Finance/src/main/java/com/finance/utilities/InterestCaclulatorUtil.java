package com.finance.utilities;

import java.time.LocalDateTime;
import java.time.Period;

public class InterestCaclulatorUtil {

	public static int getYears(LocalDateTime fromDate, LocalDateTime toDate) {
		LocalDateTime closestFullDaysStart = LocalDateTime.of(
				fromDate.toLocalDate().plusDays(toDate.toLocalTime().compareTo(fromDate.toLocalTime()) < 0 ? 1 : 0),
				toDate.toLocalTime());

		// Get the calendar period between the dates (full years, months & days).
		Period period = Period.between(closestFullDaysStart.toLocalDate(), toDate.toLocalDate());

		return period.getYears();
	}
	
	public static int getMonths(LocalDateTime fromDate, LocalDateTime toDate) {
		LocalDateTime closestFullDaysStart = LocalDateTime.of(
				fromDate.toLocalDate().plusDays(toDate.toLocalTime().compareTo(fromDate.toLocalTime()) < 0 ? 1 : 0),
				toDate.toLocalTime());

		// Get the calendar period between the dates (full years, months & days).
		Period period = Period.between(closestFullDaysStart.toLocalDate(), toDate.toLocalDate());

		return period.getMonths();
	}
	
	public static int getDays(LocalDateTime fromDate, LocalDateTime toDate) {
		LocalDateTime closestFullDaysStart = LocalDateTime.of(
				fromDate.toLocalDate().plusDays(toDate.toLocalTime().compareTo(fromDate.toLocalTime()) < 0 ? 1 : 0),
				toDate.toLocalTime());

		// Get the calendar period between the dates (full years, months & days).
		Period period = Period.between(closestFullDaysStart.toLocalDate(), toDate.toLocalDate());

		return period.getDays();
	}
}
