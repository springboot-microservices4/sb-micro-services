package com.user.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.user.entities.UserDet;

public interface UserRepository extends JpaRepository<UserDet, Integer> {

	UserDet findByUserId(Integer userId);

}
