package com.user.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class UserConfig {

	@Value("${expense.base.url}")
	private String expenseAppBaseURL;
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
	
	@Bean
	public WebClient webCLient() {
		return WebClient.builder().baseUrl(expenseAppBaseURL).build();
	}
	
}
