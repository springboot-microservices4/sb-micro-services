package com.user.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.user.models.CommonResponse;
import com.user.models.UserModel;
import com.user.services.UserService;

@RestController
public class UserController {

	@Autowired
	private UserService expenseService;

	@GetMapping("/v1/get/{userId}")
	public CommonResponse<UserModel> getBalanceStatement(@PathVariable("userId") Integer userId) {
		return expenseService.getUserById(userId);
	}
	
	@PostMapping("/v1/post/save")
	public CommonResponse<Boolean> saveTransaction(@RequestBody UserModel bsModel) {
		return expenseService.saveUser(bsModel);
	}
}
