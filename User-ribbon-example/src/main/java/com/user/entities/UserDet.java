package com.user.entities;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author Manikanta
 * @since 12/08/2023
 */
@Entity
@Data
@Table(name = "users")
public class UserDet {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="user_id")
	private int userId;
	
	@Column(name="first_name", length = 50)
	private String firstName;
	
	@Column(name="last_name", length = 50)
	private String lastName;
	
	@Column(name="user_email", length = 100)
	private String userEmail;
	
	@Column(name="mobile_no", length = 15)
	private String mobileNo;
	
	@Column(name="is_deleted", length = 1)//1: not deleted, 0: deleted
	private String isDeleted;
	
	@Column(name="user_password", length = 100)
	private String userPassword;
	
	@Column(name="created_date")
	private LocalDateTime createdDate;
	
	@Column(name="modified_date")
	private LocalDateTime modifiedDate;
}
