package com.user.models;

import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserStatementModel {

	private int userId;
	private Double finalBalance;
	private Double totalInwards;
	private Double totalOutWards;
	private List<BalanceStatementModel> bsModel;
	
}
