package com.user.services;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.user.entities.UserDet;
import com.user.models.CommonResponse;
import com.user.models.UserModel;
import com.user.models.UserStatementModel;
import com.user.repositories.UserRepository;
import com.user.utilities.APIService;
import com.user.utilities.UserConstants;

@Service
public class UserService {

	DateTimeFormatter fomatter = DateTimeFormatter.ofPattern(UserConstants.TIME_FORMAT);

	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private APIService apiService;

	public CommonResponse<UserModel> getUserById(Integer userId) {
		CommonResponse<UserModel> response = new CommonResponse<UserModel>();
		UserModel userModel = new UserModel();
		try {
			UserDet userDet = userRepo.findByUserId(userId);
			BeanUtils.copyProperties(userDet, userModel);
			userModel.setCreatedDate(userDet.getCreatedDate().format(fomatter));
			userModel.setModifiedDate(userDet.getModifiedDate().format(fomatter));

//			UserStatementModel userStatement = apiService.getUserStatementUsingRestTemplate(userId);
//			UserStatementModel userStatement = apiService.getUserStatementUsingWebClient(userId);
			
			CommonResponse<UserStatementModel> userStatement = apiService.getUserStatementUsingFeignClient(userId);
			
			userModel.setUserStatement(userStatement.getResponse());
			response.setMessage("Success");
			response.setResponse(userModel);
			response.setStatusCode(200);
		} catch (Exception ex) {
			response.setMessage("Fetch Failed " + ex.getMessage());
			response.setResponse(null);
			response.setStatusCode(500);
		}
		return response;
	}

	public CommonResponse<Boolean> saveUser(UserModel userModel) {
		CommonResponse<Boolean> response = new CommonResponse<Boolean>();
		try {
			UserDet userDet = new UserDet();
			if (userModel != null) {
				BeanUtils.copyProperties(userModel, userDet);
			}
			userDet.setIsDeleted("1");
			userDet.setCreatedDate(LocalDateTime.now());
			userDet.setModifiedDate(LocalDateTime.now());

			userRepo.save(userDet);

			response.setMessage("User Saved Successfully");
			response.setResponse(true);
			response.setStatusCode(200);
		} catch (Exception ex) {
			response.setMessage("User Saving Failed " + ex.getMessage());
			response.setResponse(false);
			response.setStatusCode(500);
		}
		return response;
	}

}
