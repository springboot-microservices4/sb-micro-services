package com.user.utilities;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import com.user.models.CommonResponse;
import com.user.models.UserStatementModel;

@Component
public class APIService {

	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private WebClient webClient;
	
	@Autowired
	private ExpenseFeignClient feignClient;

	//Restemplate is blocking in nature
	public UserStatementModel getUserStatementUsingRestTemplate(Integer userId) {
		return restTemplate.getForObject(
				"http://localhost:8081/expense/api/v1/get/balance-statement/{userId}",UserStatementModel.class, userId);
	}

	//Web client is Non blocking in nature
	public UserStatementModel getUserStatementUsingWebClient(Integer userId) {
		return webClient.get().uri("/v1/get/balance-statement/"+userId).retrieve().bodyToMono(UserStatementModel.class).block();
	}

	public CommonResponse<UserStatementModel> getUserStatementUsingFeignClient(Integer userId) {
		return feignClient.getUserStatementByUserId(userId);
	}
}
