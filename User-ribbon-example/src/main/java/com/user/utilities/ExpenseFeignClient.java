package com.user.utilities;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.user.models.CommonResponse;
import com.user.models.UserStatementModel;

@FeignClient(name="expense-client", path="/expense/api")
@RibbonClient(name="expense-client")
public interface ExpenseFeignClient {

	@GetMapping("/v1/get/balance-statement/{userId}")
	CommonResponse<UserStatementModel> getUserStatementByUserId(@PathVariable("userId") Integer userId);
}
