package com.user.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.user.models.AuthRequest;
import com.user.models.AuthResponse;
import com.user.models.CommonResponse;
import com.user.models.UserModel;
import com.user.services.UserService;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.jsonwebtoken.Jwt;

@RestController
public class UserController {

	@Autowired
	private UserService userService;
	
	@Autowired
    private AuthenticationManager authenticationManager;

	@GetMapping("/v1/get/{userId}")
	@CircuitBreaker(name="expense", fallbackMethod = "fallbackMethod")
	public CommonResponse<UserModel> getBalanceStatement(@PathVariable("userId") Integer userId) {
		return userService.getUserById(userId);
	}
	
	@PostMapping("/v1/post/save")
	public CommonResponse<Boolean> saveTransaction(@RequestBody UserModel bsModel) {
		return userService.saveUser(bsModel);
	}
	
	public CommonResponse<UserModel> fallbackMethod(Integer userId, RuntimeException runtimeException) {
		CommonResponse<UserModel> res = new CommonResponse<UserModel>();
		UserModel statement = new UserModel();
//		return "Apolozise!!, Currently we are unable to reach this service, please try again later.";
		res.setMessage("Apolozise!!, Currently we are unable to reach this service, please try again later. " + runtimeException.getMessage());
		res.setStatusCode(200);
		res.setResponse(statement);
		return res;
	}
	
	@PostMapping("/oauth/register")
    public CommonResponse<Boolean> addNewUser(@RequestBody UserModel user) {
        return userService.saveUser(user);
    }

    @PostMapping("/oauth/token")
    public CommonResponse<AuthResponse> getToken(@RequestBody AuthRequest authRequest) {
        Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword()));
        if (authenticate.isAuthenticated()) {
            return userService.generateToken(authRequest.getUsername());
        } else {
            throw new RuntimeException("invalid access");
        }
    }
    
    @GetMapping("/get-current-user")
    public CommonResponse<UserModel> getCurrentUser() {
//    	userService.validateToken();
        return userService.getCurrentUser();
    }

    @GetMapping("/oauth/validate")
    public String validateToken(@RequestParam("token") String token) {
    	userService.validateToken();
        return "Token is valid";
    }
}
