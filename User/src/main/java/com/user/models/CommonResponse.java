package com.user.models;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CommonResponse<T> {
	
	private int statusCode;
	
	private String message;
	
	private T response;

	
}
