package com.user.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.user.entities.UserDet;

public interface UserRepository extends JpaRepository<UserDet, Integer> {

	UserDet findByUserId(Integer userId);

	Optional<UserDet> findByUserEmail(String username);

}
