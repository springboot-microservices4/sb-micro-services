package com.user.services;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.user.entities.UserDet;
import com.user.models.AuthResponse;
import com.user.models.CommonResponse;
import com.user.models.UserModel;
import com.user.models.UserStatementModel;
import com.user.repositories.UserRepository;
import com.user.utilities.APIService;
import com.user.utilities.UserConstants;

import jakarta.servlet.http.HttpServletRequest;

@Service
public class UserService {

	DateTimeFormatter fomatter = DateTimeFormatter.ofPattern(UserConstants.TIME_FORMAT);

	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private APIService apiService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	 @Autowired
	    private JWTService jwtService;
	 
	 @Autowired
	 private HttpServletRequest httpRequest;
	 

	public CommonResponse<UserModel> getUserById(Integer userId) {
		CommonResponse<UserModel> response = new CommonResponse<UserModel>();
		UserModel userModel = new UserModel();
		try {
			UserDet userDet = userRepo.findByUserId(userId);
			BeanUtils.copyProperties(userDet, userModel);
			userModel.setCreatedDate(userDet.getCreatedDate().format(fomatter));
			userModel.setModifiedDate(userDet.getModifiedDate().format(fomatter));

//			CommonResponse<UserStatementModel> userStatement = apiService.getUserStatementUsingRestTemplate(userId);
//			UserStatementModel userStatement = apiService.getUserStatementUsingWebClient(userId);
			
//			CommonResponse<UserStatementModel> userStatement = apiService.getUserStatementUsingFeignClient(userId);
			
//			if(userStatement.getStatusCode() == 200) {
//				userModel.setUserStatement(userStatement.getResponse());
//			} else {
//				throw new RuntimeException(userStatement.getMessage());
//			}
			response.setMessage("Success");
			response.setResponse(userModel);
			response.setStatusCode(200);
		} catch (Exception ex) {
			response.setMessage("Fetch Failed " + ex.getMessage());
			response.setResponse(null);
			response.setStatusCode(500);
			throw new RuntimeException(ex.getMessage());
		}
		return response;
	}

	public CommonResponse<Boolean> saveUser(UserModel userModel) {
		CommonResponse<Boolean> response = new CommonResponse<Boolean>();
		try {
			UserDet userDet = new UserDet();
			if (userModel != null) {
				BeanUtils.copyProperties(userModel, userDet);
			}
			userDet.setIsDeleted("1");
			userDet.setCreatedDate(LocalDateTime.now());
			userDet.setModifiedDate(LocalDateTime.now());
			userDet.setUserPassword(passwordEncoder.encode(userModel.getUserPassword()));
			userRepo.save(userDet);

			response.setMessage("User Saved Successfully");
			response.setResponse(true);
			response.setStatusCode(200);
		} catch (Exception ex) {
			response.setMessage("User Saving Failed " + ex.getMessage());
			response.setResponse(false);
			response.setStatusCode(500);
		}
		return response;
	}
	
	public CommonResponse<AuthResponse> generateToken(String username) {
		CommonResponse<AuthResponse> response = new CommonResponse<AuthResponse>();
		AuthResponse tokenRes = new AuthResponse();
		tokenRes.setAccess_token(jwtService.generateToken(username));
		response.setMessage("Token Fetched");
		response.setResponse(tokenRes);
		response.setStatusCode(200);
        return response;
    }

    public void validateToken() {
    	String token = httpRequest.getHeader(HttpHeaders.AUTHORIZATION);
        jwtService.validateToken(token.split(" ")[1]);
    }

	public CommonResponse<UserModel> getCurrentUser() {
		CommonResponse<UserModel> response = new CommonResponse<UserModel>();
		UserModel userModel = new UserModel();
		try {
			String token = httpRequest.getHeader(HttpHeaders.AUTHORIZATION);
			String usernameFromToken = jwtService.getUsernameFromToken(token.split(" ")[1]);
			Optional<UserDet> findByUserEmail = userRepo.findByUserEmail(usernameFromToken);
			if(findByUserEmail.isPresent()) {
				UserDet userDet = findByUserEmail.get();
				BeanUtils.copyProperties(userDet, userModel);
				userModel.setUserPassword("");
				userModel.setCreatedDate(userDet.getCreatedDate().format(fomatter));
				userModel.setModifiedDate(userDet.getModifiedDate().format(fomatter));
				response.setMessage("Success");
				response.setResponse(userModel);
				response.setStatusCode(200);
			} else {
				response.setMessage("User Not exists");
				response.setResponse(null);
				response.setStatusCode(500);
				throw new RuntimeException("User Not exists");
			}
		} catch (Exception ex) {
			response.setMessage("Fetch Failed " + ex.getMessage());
			response.setResponse(null);
			response.setStatusCode(500);
			throw new RuntimeException(ex.getMessage());
		}
		return response;
	}

}
