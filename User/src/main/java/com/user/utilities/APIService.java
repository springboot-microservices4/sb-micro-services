package com.user.utilities;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import com.user.models.CommonResponse;
import com.user.models.UserStatementModel;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class APIService {

	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private WebClient webClient;
	
	@Autowired
	private ExpenseFeignClient feignClient;
	
//	@Autowired
//	private DiscoveryClient discoveryClient;
	
//	@Autowired
//	private LoadBalancerClient loadBalanceClient;

	//Restemplate is blocking in nature
	@SuppressWarnings("unchecked")
	public CommonResponse<UserStatementModel> getUserStatementUsingRestTemplate(Integer userId) {
//		List<ServiceInstance> instances = discoveryClient.getInstances("expense");
		
//		ServiceInstance choose = loadBalanceClient.choose("expense");
//		String uri = choose.getUri().toString();
//		String context = choose.getMetadata().get("context");
//		log.info("URI:  "+ uri+context);
		return (CommonResponse<UserStatementModel>) restTemplate.getForObject(
				"http://expense/expense/api/v1/get/balance-statement/{userId}",Object.class, userId);
	}

	//Web client is Non blocking in nature
	public UserStatementModel getUserStatementUsingWebClient(Integer userId) {
		return webClient.get().uri("/v1/get/balance-statement/"+userId).retrieve().bodyToMono(UserStatementModel.class).block();
	}

	public CommonResponse<UserStatementModel> getUserStatementUsingFeignClient(Integer userId) {
		return feignClient.getUserStatementByUserId(userId);
	}
}
