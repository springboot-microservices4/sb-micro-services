package com.user.utilities;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.user.models.CommonResponse;
import com.user.models.UserStatementModel;

@FeignClient(name="expense", path="/expense/api")
public interface ExpenseFeignClient {
	
	@GetMapping("/v1/get/balance-statement/{userId}")
	CommonResponse<UserStatementModel> getUserStatementByUserId(@PathVariable("userId") Integer userId);
}
