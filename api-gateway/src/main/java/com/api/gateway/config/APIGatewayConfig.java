package com.api.gateway.config;

import org.springframework.cloud.gateway.filter.ratelimit.RedisRateLimiter;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.api.gateway.config.UserCustomFilter.Config;

@Configuration
public class APIGatewayConfig {

	@Bean
	public RouteLocator myRoutes(RouteLocatorBuilder builder, UserCustomFilter userCustomFilter) {
		//Filter reference from:  https://www.javainuse.com/spring/cloud-filter 
		//https://www.baeldung.com/spring-cloud-custom-gateway-filters
	    return builder.routes()
	        .route(p -> p.path("/user/api/**").filters(f -> f.filter(userCustomFilter.apply(new Config("My Custom Filer", true, true)))).uri("lb://user"))
	        .route(p -> p.path("/expense/api/**").filters(f -> f.addResponseHeader("Post-res-Header", "Expense Post Header")).uri("lb://expense"))
	        .route(p -> p.path("/finance/api/**").uri("lb://finance"))
	        .route(p -> p.path("/eureka/web").filters(f -> f.setPath("/")).uri("http://localhost:5000"))
	        .route(p -> p.path("/eureka/**").uri("http://localhost:5000"))
	        .build();
	}
	
//	@Bean
//	public RedisRateLimiter redisRateLimiter() {
//	    return new RedisRateLimiter(5, 5, 5);
//	}
}
