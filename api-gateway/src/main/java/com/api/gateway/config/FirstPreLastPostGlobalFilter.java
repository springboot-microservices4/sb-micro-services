package com.api.gateway.config;

import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import reactor.core.publisher.Mono;

@Configuration
public class FirstPreLastPostGlobalFilter {

	@Bean
	public GlobalFilter globalFilter() {
		return (exchange, chain) -> {
			System.out.println("Pre Global filter");
			return chain.filter(exchange).then(Mono.fromRunnable(() -> {
				System.out.println("Post Global filter");
			}));
		};
	}
}
