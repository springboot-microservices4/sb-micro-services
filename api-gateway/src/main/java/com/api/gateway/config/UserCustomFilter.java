package com.api.gateway.config;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class UserCustomFilter extends AbstractGatewayFilterFactory<UserCustomFilter.Config>{

	public UserCustomFilter() {
		super(Config.class);
	}
	
	@Override
	public GatewayFilter apply(Config config) {
		//Custom Pre Filter. Suppose we can extract JWT and perform Authentication
		return (exchange, chain) -> {
			System.out.println("First pre filter: "+config.isPreLogger()+":" + config.getBaseMessage());
			//Custom Post Filter.Suppose we can call error response handler based on error code.
			return chain.filter(exchange).then(Mono.fromRunnable(() -> {
				System.out.println("First post filter"+config.isPostLogger()+":" + config.getBaseMessage());
			}));
		};
	}

	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class Config {
		// Put the configuration properties
		private String baseMessage;
	    private boolean preLogger;
	    private boolean postLogger;
	}
}
