package com.admin.dashboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import de.codecentric.boot.admin.server.config.EnableAdminServer;

@SpringBootApplication
@EnableDiscoveryClient
@EnableAdminServer
@OpenAPIDefinition(info = @Info(title="APP-AdminDasboard", version="1.0", description = "Admin Dashboard for all microservices"))
public class MicroserviceAdminDashboardApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroserviceAdminDashboardApplication.class, args);
	}

}
