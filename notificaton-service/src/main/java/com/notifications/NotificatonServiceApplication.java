package com.notifications;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title="APP-Notifications", version="1.0", description = "Microservice for notifications to the user"))
public class NotificatonServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(NotificatonServiceApplication.class, args);
	}

}
