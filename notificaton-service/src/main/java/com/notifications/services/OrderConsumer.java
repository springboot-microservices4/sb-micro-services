package com.notifications.services;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.notifications.models.OrderEvent;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j	
public class OrderConsumer {

	@KafkaListener(topics="${spring.kafka.topic.name}", groupId = "${spring.kafka.consumer.group-id}", containerFactory = "concurrentKafkaListenerContainerFactory")
	public void consume(OrderEvent event) {
		log.info(String.format("Order notification sent => %s", event.toString()));
	}
}
