import { Routes } from '@angular/router';
import { LoginLayoutComponent } from './components/layouts/login-layout/login-layout/login-layout.component';
import { AdminLayoutComponent } from './components/layouts/admin-layout/admin-layout/admin-layout.component';
import { AuthGuard } from './authentication/auth-guard.service';
import { UserLayoutComponent } from './components/layouts/user-layout/user-layout/user-layout.component';

export const routes: Routes = [
    {path:'', redirectTo: 'login', pathMatch:'full'},
    {
        path:'login', 
        component: LoginLayoutComponent,
        loadChildren: () => import('./components/layouts/login-layout/login.routes')
        .then(m => m.loginRoutes)

    },

    {
        path:'admin', 
        component: AdminLayoutComponent,
        canActivate: [AuthGuard],
        loadChildren: () => import('./components/layouts/admin-layout/admin.routes')
        .then(m => m.adminRoutes)
    },
    {
        path:'user', 
        component: UserLayoutComponent,
        canActivate: [AuthGuard],
        loadChildren: () => import('./components/layouts/user-layout/user.routes')
        .then(m => m.userRoutes)

    }
];
