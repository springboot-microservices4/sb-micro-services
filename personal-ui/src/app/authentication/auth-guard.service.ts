import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, CanActivateChild } from '@angular/router';
import { Observable } from 'rxjs';

import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate{

  constructor(private authService: AuthService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let isLoggedIn = false;
   this.authService.currentUserSubject.subscribe(data => {
        if (data.isUserLoggedIn) {
            isLoggedIn = true
            return isLoggedIn;
        } else {
            isLoggedIn = false;
            this.router.navigate(['/login']);
        }
        return isLoggedIn;
    });

    return isLoggedIn
}

  // canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
  // boolean | Observable<boolean> | Promise<boolean> {
  //   return this.canActivate(route, state);
  // }
}

