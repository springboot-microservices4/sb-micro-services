import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpEvent,
  HttpHandler,
  HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { TokenResponse } from '../models/token-response';
import { environment } from '../../environments/environment';
import { User } from '../models/user';
@Injectable({
  providedIn: 'root',
})
export class AuthInteceptorService implements HttpInterceptor {
  constructor(private authenticationService: AuthService) {}
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // add auth header with jwt if user is logged in and request is to the api url
    let tokenResponse: TokenResponse = this.authenticationService.tokenResponse;

    let currentUser: User = this.authenticationService.user;
    const isLoggedIn = tokenResponse && tokenResponse.access_token;

    const isApiUrl = request.url;
    let token = localStorage.getItem('token_response');
    if (isLoggedIn && isApiUrl && !request.url.includes('token')) {
      request = request.clone({
        setHeaders: {
          // 'Access-Control-Allow-Origin': 'http://localhost:4200',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${tokenResponse.access_token}`,
        },
      });
    }
    //  else {
    //   request = request.clone({
    //     setHeaders: {
    //       Authorization: `Basic ${window.btoa('M@niBhavi:test123')}`
    //     }
    // });

    // }

    if (request.method) {
      console.log(request.method);
    }
    return next.handle(request);
  }
}
