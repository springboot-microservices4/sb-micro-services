import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, map, Observable } from 'rxjs';
import { TokenResponse } from '../models/token-response';
import { User } from '../models/user';
import { environment } from '../../environments/environment';
import { GET_LOGGED_IN_USER, TOKEN_URL } from '../shared/shared.consts';
import { AlertService } from '../shared/alert.service';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  LoginStatus: any;

  public currentUserSubject: BehaviorSubject<User>;
  user: User = new User();
  tokenResponse: TokenResponse = new TokenResponse();
  roles: Array<string> = new Array<string>();

  constructor(
    private http: HttpClient,
    private router: Router,
    private alertService: AlertService
  ) {
    this.currentUserSubject = new BehaviorSubject<User>(this.user);
    let tokenRes = localStorage.getItem('token_response');
    if (tokenRes != undefined && tokenRes != null) {
      this.tokenResponse = JSON.parse(tokenRes);
      let userRes = localStorage.getItem('current_user');
      if(userRes != undefined && userRes != null && userRes != '') {
        this.user = JSON.parse(userRes);
        this.user.isUserLoggedIn = true;
        this.currentUserSubject.next(this.user);
      }
    }
  }

  public get currentUserValue() {
    return this.currentUserSubject;
  }
  login(username?: any, password?: any): Observable<any> {
    let headers = new HttpHeaders();
    // headers = headers.append('Content-Type', 'application/json');
    // headers = headers.append('X-Frame-Options', 'sameorigin');
    // headers = headers.append('Access-Control-Allow-Origin', '*');
    // headers = headers.append('Authorization',`Basic ${window.btoa('M@niBhavi:test123')}`);
    headers = headers.append('Content-Type', 'application/json');

    let body = {
      username: username,
      password: password,
    };
    return this.http
      .post<any>(`${environment.tokenServerURL}${TOKEN_URL}`, body, {
        headers: headers,
      })
      .pipe(
        map((token) => {
          this.alertService.successAlert('You are signed in successfully');
          localStorage.setItem(
            'token_response',
            JSON.stringify(token.response)
          );
          this.tokenResponse = token.response;
          this.getUser().subscribe((userData) => {
            if (userData.statusCode == 200) {
              this.user = userData.response;
              localStorage.setItem('current_user', JSON.stringify(this.user));
              this.user.isUserLoggedIn = true;
              this.currentUserSubject.next(this.user);
              this.roles = userData.response.roles;
              // if(this.roles.includes('USER')) {
              this.router.navigate(['/user']);
              // }
            }
          });
        })
      );
  }

  getCurrentUser(): User {
    let user = localStorage.getItem('currentUser');
    if (user) {
      this.user = JSON.parse(user);
    }
    return this.user;
  }

  getUser(): Observable<any> {
    return this.http.get<any>(
      `${environment.apiServerURl}${GET_LOGGED_IN_USER}`
    );
  }

  logout() {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to Signout!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
    }).then((result) => {
      if (result.isConfirmed) {
        localStorage.removeItem('currentUser');
        this.user.isUserLoggedIn = false;
        this.currentUserSubject.next(this.user);
        const Toast = Swal.mixin({
          toast: true,
          position: 'bottom-end',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.onmouseenter = Swal.stopTimer;
            toast.onmouseleave = Swal.resumeTimer;
          },
        });
        Toast.fire({
          icon: 'success',
          title: 'You are logged out',
        });
      }
    });
    // remove user from local storage to log user out
  }
}
