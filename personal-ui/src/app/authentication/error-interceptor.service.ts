import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent,HttpHandler,HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { AlertService } from '../shared/alert.service';

@Injectable({
  providedIn: 'root'
})
export class ErrorInterceptorService implements HttpInterceptor{
  constructor(private authenticationService: AuthService, private alertService: AlertService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 401) {
                this.authenticationService.logout();
                location.reload();
            } else if(err.statusText === 'Unknown Error') {
              this.alertService.errorAlert('Unable to communicate with server..!');
            } else {
              this.alertService.errorAlert(err.message);
            }

            const error = err.error.message || err.statusText;
            return throwError(error);
        }))
    }
}
