import { Injectable } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  busyRequestCount = 0;
  type = "square-spin";
  // type = "ball-scale-multiple";
  // type = "ball-scale-ripple";

  constructor(private spinnerService:NgxSpinnerService) { }

  busy(){
    console.log('loadin...')
    this.busyRequestCount++;
    this.spinnerService;
    this.spinnerService.show();
  }

  idle(){
    this.busyRequestCount--;
    if(this.busyRequestCount <=0){
      this.busyRequestCount = 0;
      this.spinnerService.hide();
    }
  }
}
