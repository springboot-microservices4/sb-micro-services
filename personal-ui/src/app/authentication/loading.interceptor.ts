import { inject, Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpEvent,
  HttpHandler,
  HttpRequest,
} from '@angular/common/http';
import { delay, delayWhen, finalize, Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { LoaderService } from './loader.service';
@Injectable({
  providedIn: 'root',
})
export class loadingInterceptor implements HttpInterceptor {
  constructor(private authenticationService: AuthService) {}
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const busyService = inject(LoaderService);
  busyService.busy();
  console.log("Loading INterceptor");
  return next.handle(request).pipe(
    delay(1000),
    finalize(() => busyService.idle()));
  }
}