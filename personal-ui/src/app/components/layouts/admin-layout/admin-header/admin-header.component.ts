import { Component } from '@angular/core';
import { SharedService } from '../../../../shared/shared.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faAngleRight, faBell, faGears, faMagnifyingGlass, faMessage } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from '../../../../authentication/auth.service';
import { RouterLink, RouterLinkActive } from '@angular/router';

@Component({
  selector: 'app-admin-header',
  standalone: true,
  imports: [RouterLink, RouterLinkActive, FontAwesomeModule],
  templateUrl: './admin-header.component.html',
  styleUrl: './admin-header.component.scss'
})
export class AdminHeaderComponent {
  faMessage= faMessage;
  faAlert = faBell;
  angleRight = faAngleRight;
  admin = faGears;
  search = faMagnifyingGlass;
  constructor(public sharedService: SharedService, private authService: AuthService) {

  }

  logout() {
    this.authService.logout();
  }
  
}
