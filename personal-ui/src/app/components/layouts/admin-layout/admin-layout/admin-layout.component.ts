import { Component } from '@angular/core';
import { AdminHeaderComponent } from '../admin-header/admin-header.component';
import { AdminFooterComponent } from '../admin-footer/admin-footer.component';
import { AdminSideComponent } from '../admin-side/admin-side.component';
import { RouterOutlet } from '@angular/router';
import { NgClass } from '@angular/common';
import { SharedService } from '../../../../shared/shared.service';

@Component({
  selector: 'app-admin-layout',
  standalone: true,
  imports: [RouterOutlet, AdminHeaderComponent, AdminFooterComponent, AdminSideComponent, NgClass],
  templateUrl: './admin-layout.component.html',
  styleUrl: './admin-layout.component.scss'
})
export class AdminLayoutComponent {

  constructor(public sharedService: SharedService) {

  }
}
