import { Routes } from '@angular/router';
import { CardDetailsComponent } from './card-details/card-details.component';
import { CategoriesComponent } from './categories/categories.component';
import { PaymentTypesComponent } from './payment-types/payment-types.component';

export const adminRoutes: Routes = [
    {path:'', redirectTo: 'card-details', pathMatch:'full'},
    {
        path:'card-details', 
        component: CardDetailsComponent
    },
    {
        path:'categories', 
        component: CategoriesComponent
    },
    {
        path:'payment-types', 
        component: PaymentTypesComponent
    }
];
