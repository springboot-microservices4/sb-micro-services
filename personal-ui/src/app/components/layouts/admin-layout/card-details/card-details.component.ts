import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpService } from '../../../../shared/http.service';
import { AlertService } from '../../../../shared/alert.service';
import { AuthService } from '../../../../authentication/auth.service';

@Component({
  selector: 'app-card-details',
  templateUrl: './card-details.component.html',
  styleUrls: ['./card-details.component.scss'],
  standalone: true,
  imports: [FormsModule, ReactiveFormsModule, CommonModule]
})
export class CardDetailsComponent  implements OnInit {

  cardDetailsList: any = new  Array<any>();
  cardDetails: any = {};
  constructor(private httpService: HttpService, private alertService: AlertService, private authService: AuthService) { }

  ngOnInit() {
    this.getCardDetailsList();
  }

  onSubmit() {
    console.log(JSON.stringify(this.cardDetails));

    this.cardDetails.userId = this.authService.getCurrentUser().userId

  this.httpService.saveCardDetails(this.cardDetails).subscribe(data => {
    this.alertService.successAlert('Card Details Saved');
    this.getCardDetailsList();
  })
  }

  getCardDetailsList() {
    this.httpService.getCardDetailsList(this.authService.getCurrentUser().userId).subscribe(data => {
      if(data) {
        this.cardDetailsList = data.response;
        this.httpService.updateCardDetailsList(this.cardDetailsList);
      }
    });
  }

}
