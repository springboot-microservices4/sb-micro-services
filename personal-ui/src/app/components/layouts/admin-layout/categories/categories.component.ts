import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpService } from '../../../../shared/http.service';
import { AlertService } from '../../../../shared/alert.service';
import { AuthService } from '../../../../authentication/auth.service';
import $ from 'jquery';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss'],
  standalone: true,
  imports: [FormsModule, ReactiveFormsModule, CommonModule],
})
export class CategoriesComponent implements OnInit {
  categoriesList: any = new Array<any>();
  category: any = {};
  constructor(
    private httpService: HttpService,
    private alertService: AlertService,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.getCardDetailsList();
  }

  onSubmit() {
    // $('#staticBackdrop').modal('hide');
    // $("#staticBackdrop").trigger('click');
    console.log(JSON.stringify(this.category));

    this.category.userId = this.authService.getCurrentUser().userId;

    this.httpService.saveCategory(this.category).subscribe((data) => {
      // $('#staticBackdrop').modal().hide();
      this.alertService.successAlert('Category Saved');
      this.getCardDetailsList();
    });
  }

  getCardDetailsList() {
    this.httpService
      .getCategories(this.authService.getCurrentUser().userId)
      .subscribe((data) => {
        if (data) {
          this.categoriesList = data.response;
          this.httpService.updateCardDetailsList(this.categoriesList);
          this.httpService.cardDetailsListBsList.subscribe(data => {
            this.categoriesList = data;
          })
        }
      });
  }

  makeCategoryValue() {
    if (
      this.category.categoryName !== undefined &&
      this.category.categoryName !== null &&
      this.category.categoryName !== ''
    ) {
      this.category.categoryValue = this.category.categoryName
        .trim()
        .toUpperCase()
        .replaceAll(' ', '_');
    }
  }

  clearModal() {
    this.category = {};
  }
}
