import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpService } from '../../../../shared/http.service';
import { AlertService } from '../../../../shared/alert.service';
import { AuthService } from '../../../../authentication/auth.service';

@Component({
  selector: 'app-payment-types',
  templateUrl: './payment-types.component.html',
  styleUrls: ['./payment-types.component.scss'],
  standalone: true,
  imports: [FormsModule, ReactiveFormsModule, CommonModule]
})
export class PaymentTypesComponent  implements OnInit {

  paymentTypeList: any = new  Array<any>();
  paymentTypeModel: any = {};
  constructor(private httpService: HttpService, private alertService: AlertService, private authService: AuthService) { }

  ngOnInit() {
    this.getCardDetailsList();
  }

  onSubmit() {
    console.log(JSON.stringify(this.paymentTypeModel));

    this.paymentTypeModel.userId = this.authService.getCurrentUser().userId

  this.httpService.savePaymentType(this.paymentTypeModel).subscribe(data => {
    this.alertService.successAlert('Card Details Saved');
    this.getCardDetailsList();
  })
  }

  getCardDetailsList() {
    this.httpService.getPaymentTypes(this.authService.getCurrentUser().userId).subscribe(data => {
      if(data) {
        this.paymentTypeList = data.response;
        this.httpService.updatePaymentTypes(data.response);
      }
    })
  }

  makeCategoryValue() {
    if (
      this.paymentTypeModel.paymentType !== undefined &&
      this.paymentTypeModel.paymentType !== null &&
      this.paymentTypeModel.paymentType !== ''
    ) {
      this.paymentTypeModel.paymentTypeValue = this.paymentTypeModel.paymentType
        .trim()
        .toUpperCase()
        .replaceAll(' ', '_');
    }
  }

  clearModal() {
    this.paymentTypeModel = {};
  }
}
