import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { LoginFooterComponent } from "../login-footer/login-footer.component";

@Component({
  selector: 'app-login-layout',
  standalone: true,
  imports: [RouterOutlet, LoginFooterComponent],
  templateUrl: './login-layout.component.html',
  styleUrl: './login-layout.component.scss'
})
export class LoginLayoutComponent {

}
