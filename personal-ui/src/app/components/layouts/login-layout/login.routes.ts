import { Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';

export const loginRoutes: Routes = [
    {path:'', redirectTo: 'login', pathMatch:'full'},
    {
        path:'', 
        component: LoginComponent
    }
];
