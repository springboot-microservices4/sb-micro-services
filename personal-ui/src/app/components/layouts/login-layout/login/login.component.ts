import { Component } from '@angular/core';
import { FormsModule, NgModel, ReactiveFormsModule } from '@angular/forms';
import { RouterLink } from '@angular/router';
import { AuthService } from '../../../../authentication/auth.service';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [RouterLink, FormsModule, ReactiveFormsModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent {
  isRegister: boolean = false;
  username?: any;
  password?:any;
constructor(private authService: AuthService) {
  
}
  toggleLogin() {
    this.isRegister = !this.isRegister;
  }

  login() {
    this.authService.login(this.username, this.password).subscribe((data : any) =>{
      console.log(data)
    });
    }
}

