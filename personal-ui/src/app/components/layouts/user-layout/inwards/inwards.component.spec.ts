import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InwardsComponent } from './inwards.component';

describe('InwardsComponent', () => {
  let component: InwardsComponent;
  let fixture: ComponentFixture<InwardsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InwardsComponent ],
      imports: []
    }).compileComponents();

    fixture = TestBed.createComponent(InwardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
