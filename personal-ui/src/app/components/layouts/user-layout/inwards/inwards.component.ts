import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormsModule, NgModel, ReactiveFormsModule } from '@angular/forms';
import { NgSelectComponent, NgSelectModule } from '@ng-select/ng-select';
import {
  BsDatepickerConfig,
  BsDatepickerModule,
} from 'ngx-bootstrap/datepicker';
import { HttpService } from '../../../../shared/http.service';
import { AlertService } from '../../../../shared/alert.service';
import { CARD_TRANSACTION_TYPES, DATE_CONFIG, TRANSACTION_TYPES } from '../../../../shared/shared.consts';
import { AuthService } from '../../../../authentication/auth.service';
import { AgGridAngular } from 'ag-grid-angular';
import { ColDef } from 'ag-grid-community';
import { BalanceSheetModel } from '../../../../models/user-balance-sheet';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';

@Component({
  selector: 'app-inwards',
  standalone: true,
  templateUrl: './inwards.component.html',
  styleUrls: ['./inwards.component.scss'],
  imports: [
    FormsModule,
    NgSelectModule,
    ReactiveFormsModule,
    CommonModule,
    AgGridAngular,
    BsDatepickerModule,
  ],
})
export class InwardsComponent implements OnInit {
  datepickerModel: any;
  transactionModel: any = {};
  transactionTypes = TRANSACTION_TYPES;
  cardTransactionTypes = CARD_TRANSACTION_TYPES;
  bsConfig = DATE_CONFIG;
  balanceStatement: BalanceSheetModel = new BalanceSheetModel();
  transactions: any;
  categories: Array<any> = new Array<any>();
  paymentTypes: Array<any> = new Array<any>();
  cardDetailsList: Array<any> = new Array<any>();

  categoryFieldValue: any;
  paymentTypeFieldValue: any;
  cardDetailsFieldValue: any;

  isCardDetailsEnable: boolean = false;

  link = 'link';
        items: any[] = [
            { title: 'Orangies', link: 'https://www.github.com/isahohieku' },
            { title: 'Apple', link: 'https://www.github.com/isahohieku' },
            { title: 'Mango', link: 'https://www.github.com/isahohieku' },
            { title: 'Carrot', link: 'https://www.github.com/isahohieku' }
        ];  

  constructor(
    public httpservice: HttpService,
    private alertService: AlertService,
    private authService: AuthService
  ) {
    // this.minDate.setDate(this.minDate.getDate() - 1);
    // this.maxDate.setDate(this.maxDate.getDate() + 7);
    // this.bsRangeValue = [this.bsValue, this.maxDate];

    this.httpservice.categoriesBsList.subscribe(data => {
      this.categories = data;
      console.log("Categories: "+JSON.stringify(this.categories))
    });

    this.httpservice.paymentTypesBsList.subscribe(data => {
      this.paymentTypes = data;
      console.log("Payment Types: "+JSON.stringify(this.paymentTypes))
    });

    this.httpservice.cardDetailsListBsList.subscribe(data => {
      this.cardDetailsList = data;
      console.log("Card Details: "+JSON.stringify(this.cardDetailsList))
    });
  }


  ngOnInit() {
    this.getTransactions();
    this.httpservice.userExpenseBalanceSheet.subscribe((data) => {
      this.balanceStatement = data;
    });
  }

  onSubmit() {
    this.transactionModel.transactionDate = this.httpservice.changeDateFormat(this.transactionModel.transactionDate, DATE_CONFIG.dateInputFormat)
    console.log(this.transactionModel);
    if (this.transactionModel.amtType === 'CREDIT') {
      this.transactionModel.inwardAmt = this.transactionModel.amount;
      this.transactionModel.outwardAmt = 0;
    } else {
      this.transactionModel.outwardAmt = this.transactionModel.amount;
      this.transactionModel.inwardAmt = 0;
    }

    this.transactionModel.userId = this.authService.getCurrentUser().userId;

    this.httpservice
      .saveTransaction(this.transactionModel)
      .subscribe((data) => {
        this.alertService.successAlert('Transaction Saved');
        this.getTransactions();
      });
  }

  getTransactions() {
    this.httpservice
      .getAllTransactions(this.authService.getCurrentUser().userId)
      .subscribe((data) => {
        if (data) {
          this.httpservice.updateExpenseBalanceSheet(data.response);
          this.alertService.successAlert('Transaction Fetched');
        }
      });
  }

  changePaymentType(pt: any) {
    this.transactionModel.paymentType = pt;
    if(pt.paymentTypeValue === "CREDIT_CARD") {
      this.isCardDetailsEnable = true;
    } else {
      this.isCardDetailsEnable = false;
    }
  }

  changeCategory(cg: any) {
    this.transactionModel.category = cg;
  }

  changeCardDetails(cd: any) {
    this.transactionModel.cardDetails = cd;
  }

  clearModel() {
    this.transactionModel = {};
    this.transactionModel.cardDetails = undefined;
    this.transactionModel.category = undefined;
    this.transactionModel.paymentType = undefined;
  }
  
}
