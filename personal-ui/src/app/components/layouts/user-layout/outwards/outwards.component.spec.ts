import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OutwardsComponent } from './outwards.component';

describe('OutwardsComponent', () => {
  let component: OutwardsComponent;
  let fixture: ComponentFixture<OutwardsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OutwardsComponent ],
      imports: []
    }).compileComponents();

    fixture = TestBed.createComponent(OutwardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
