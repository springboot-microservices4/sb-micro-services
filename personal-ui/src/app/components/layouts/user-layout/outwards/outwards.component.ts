import { NgIf } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { HttpService } from '../../../../shared/http.service';
import { AuthService } from '../../../../authentication/auth.service';
import { AlertService } from '../../../../shared/alert.service';
import { FinanceStatement } from '../../../../models/finance-statement';

@Component({
  selector: 'app-outwards',
  standalone: true,
  templateUrl: './outwards.component.html',
  styleUrls: ['./outwards.component.scss'],
  imports: [FormsModule, NgSelectModule],
})
export class OutwardsComponent implements OnInit {
  datepickerModel: any;
  transactionModel: any = {};
  transactionTypes = [
    { label: 'Credit', value: 'credit' },
    { label: 'Debit', value: 'debit' },
  ];

  constructor(public httpservice: HttpService, private authService: AuthService, private alertService: AlertService) {

  }

  financeStatement: FinanceStatement = new FinanceStatement();

  ngOnInit() {
    this.getFinanceStatement();
    this.httpservice.userFinianceStatement.subscribe((data: any) => {
      this.financeStatement = data;
    })
  }

  onSubmit() {
    console.log(this.transactionModel);
  }

  getFinanceStatement() {
    this.httpservice.getFinanceStatement(this.authService.getCurrentUser().userId).subscribe(data => {
      if(data) {
        this.httpservice.updateFinanceStatement(data.response);
      }
    })
  }
}
