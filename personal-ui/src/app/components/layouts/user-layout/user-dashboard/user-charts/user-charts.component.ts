import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { CanvasJSAngularChartsModule } from '@canvasjs/angular-charts';
import { HttpService } from '../../../../../shared/http.service';
import { AuthService } from '../../../../../authentication/auth.service';
import { BalanceSheetModel } from '../../../../../models/user-balance-sheet';
import { FinanceStatement } from '../../../../../models/finance-statement';

@Component({
  selector: 'app-user-charts',
  templateUrl: './user-charts.component.html',
  styleUrls: ['./user-charts.component.scss'],
  standalone: true,
  imports: [CanvasJSAngularChartsModule]
})
export class UserChartsComponent  implements OnInit, OnChanges {

  @Input() chartType = "spline"
  chart: any;
  chartOptions: any = {}
  constructor(private httpService: HttpService, private authService: AuthService) { }
  
  balanceStatement: BalanceSheetModel = new BalanceSheetModel();
  financeStatement: FinanceStatement = new FinanceStatement();

  ngOnInit() {
    
    this.httpService.userExpenseBalanceSheet.subscribe(data => {
      this.balanceStatement = data;
      if(this.chartType === 'spline') {
        this.splineChartOptions();
      } else if(this.chartType === 'column') {
        this.columnChartOptions();
      }else if(this.chartType === 'line') {
        this.dashedLinechart();
      }
    });

    this.httpService.userFinianceStatement.subscribe((data: any) => {
      this.financeStatement = data;
      if(this.chartType === 'doughnut') {
        this.doughnutChartOptions();
      }
    })

  }
  dashedLinechart() {
    this.chartOptions = {
      animationEnabled: true,
      theme: "light2",
      title:{
        text: "Site Traffic"
      },
      axisX:{
        valueFormatString: "DD MMM",
        crosshair: {
          enabled: true,
          snapToDataPoint: true
        }
      },
      axisY: {
        title: "Number of Visits",
        crosshair: {
          enabled: true
        }
      },
      toolTip:{
        shared:true
      },  
      legend:{
        cursor: "pointer",
        verticalAlign: "bottom",
        horizontalAlign: "right",
        dockInsidePlotArea: true,
        itemclick: function(e: any) {
          if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
          } else{
            e.dataSeries.visible = true;
          }
          e.chart.render();
        }
      },
      data: [{
        type: "line",
        showInLegend: true,
        name: "Total Visit",
        lineDashType: "dash",
        markerType: "square",
        xValueFormatString: "DD MMM, YYYY",
        dataPoints: [
          { x: new Date(2022, 0, 3), y: 650 },
          { x: new Date(2022, 0, 4), y: 700 },
          { x: new Date(2022, 0, 5), y: 710 },
          { x: new Date(2022, 0, 6), y: 658 },
          { x: new Date(2022, 0, 7), y: 734 },
          { x: new Date(2022, 0, 8), y: 963 },
          { x: new Date(2022, 0, 9), y: 847 },
          { x: new Date(2022, 0, 10), y: 853 },
          { x: new Date(2022, 0, 11), y: 869 },
          { x: new Date(2022, 0, 12), y: 943 },
          { x: new Date(2022, 0, 13), y: 970 },
          { x: new Date(2022, 0, 14), y: 869 },
          { x: new Date(2022, 0, 15), y: 890 },
          { x: new Date(2022, 0, 16), y: 930 }
        ]
      },
      {
        type: "line",
        showInLegend: true,
        name: "Unique Visit",
        lineDashType: "dot",
        dataPoints: [
          { x: new Date(2022, 0, 3), y: 510 },
          { x: new Date(2022, 0, 4), y: 560 },
          { x: new Date(2022, 0, 5), y: 540 },
          { x: new Date(2022, 0, 6), y: 558 },
          { x: new Date(2022, 0, 7), y: 544 },
          { x: new Date(2022, 0, 8), y: 693 },
          { x: new Date(2022, 0, 9), y: 657 },
          { x: new Date(2022, 0, 10), y: 663 },
          { x: new Date(2022, 0, 11), y: 639 },
          { x: new Date(2022, 0, 12), y: 673 },
          { x: new Date(2022, 0, 13), y: 660 },
          { x: new Date(2022, 0, 14), y: 562 },
          { x: new Date(2022, 0, 15), y: 643 },
          { x: new Date(2022, 0, 16), y: 570 }
        ]
      }]
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    // if(this.chartType === 'spline') {
    //   this.splineChartOptions();
    // } else if(this.chartType === 'doughnut') {
    //   this.doughnutChartOptions();
    // }else if(this.chartType === 'column') {
    //   this.columnChartOptions();
    // }else if(this.chartType === 'line') {
    //   this.dashedLinechart();
    // }
  }

  doughnutChartOptions() {
    this.chartOptions = {
      animationEnabled: true,
      title:{
      text: "Finance"
      },
      data: [{
      type: "doughnut",
      yValueFormatString: "#,###.##",
      indexLabel: "{name}",
      dataPoints: [
        { y: this.financeStatement.totalAmount, name: `Total Amount (${this.financeStatement.totalAmount})` },
        { y:  this.financeStatement.totalInterest , name: `Interest (${this.financeStatement.totalInterest})` },
        { y: this.financeStatement.totalPriciple, name: `Principle (${this.financeStatement.totalPriciple})` },
      ]
      }]
    }
  }

  splineChartOptions() {
    this.chartOptions = {
      animationEnabled: true,
      theme: 'light2',
      text: "Expenses",
      axisY: {
        title: 'Number of Orders',
        includeZero: true,
      },
      axisY2: {
        title: 'Total Revenue',
        includeZero: true,
        labelFormatter: (e: any) => {
          var suffixes = ['', 'K', 'M', 'B'];
  
          var order = Math.max(Math.floor(Math.log(e.value) / Math.log(1000)), 0);
          if (order > suffixes.length - 1) order = suffixes.length - 1;
  
          var suffix = suffixes[order];
          return '$' + e.value / Math.pow(1000, order) + suffix;
        },
      },
      toolTip: {
        shared: true,
      },
      legend: {
        cursor: 'pointer',
        itemclick: function (e: any) {
          if (
            typeof e.dataSeries.visible === 'undefined' ||
            e.dataSeries.visible
          ) {
            e.dataSeries.visible = false;
          } else {
            e.dataSeries.visible = true;
          }
          e.chart.render();
        },
      },
      data: [
        {
          type: 'column',
          showInLegend: true,
          name: 'Revenue',
          axisYType: 'secondary',
          yValueFormatString: '$#,###',
          dataPoints: [
            { label: 'Jan', y: 250000 },
            { label: 'Feb', y: 431000 },
            { label: 'Mar', y: 646000 },
            { label: 'Apr', y: 522000 },
            { label: 'May', y: 464000 },
            { label: 'Jun', y: 470000 },
            { label: 'Jul', y: 534000 },
            { label: 'Aug', y: 407000 },
            { label: 'Sep', y: 484000 },
            { label: 'Oct', y: 465000 },
            { label: 'Nov', y: 424000 },
            { label: 'Dec', y: 231000 },
          ],
        },
        {
          type: this.chartType,
          showInLegend: true,
          name: 'No of Orders',
          dataPoints: [
            { label: 'Jan', y: 372 },
            { label: 'Feb', y: 412 },
            { label: 'Mar', y: 572 },
            { label: 'Apr', y: 224 },
            { label: 'May', y: 246 },
            { label: 'Jun', y: 601 },
            { label: 'Jul', y: 642 },
            { label: 'Aug', y: 590 },
            { label: 'Sep', y: 527 },
            { label: 'Oct', y: 273 },
            { label: 'Nov', y: 251 },
            { label: 'Dec', y: 331 },
          ],
        },
      ],
    };
  }

  columnChartOptions() {
    this.chartOptions = {
      title:{
        text: "Angular Column Chart"  
      },
      animationEnabled: true,
      data: [{        
        type: "column",
        dataPoints: [
          { x: 10, y: 71 },
          { x: 20, y: 55 },
          { x: 30, y: 50 },
          { x: 40, y: 65 },
          { x: 50, y: 95 },
          { x: 60, y: 68 },
          { x: 70, y: 28 },
          { x: 80, y: 34 },
          { x: 90, y: 14 }
        ]
      }]
    }
  }
   

}
