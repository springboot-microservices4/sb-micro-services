import { Component, ElementRef, ViewChild } from '@angular/core';
import { HttpService } from '../../../../shared/http.service';
import { AlertService } from '../../../../shared/alert.service';
import { AuthService } from '../../../../authentication/auth.service';

import { GridStack, GridStackOptions, GridStackWidget } from 'gridstack';

// NOTE: local testing of file
// import { GridstackComponent, NgGridStackOptions, NgGridStackWidget, elementCB, gsCreateNgComponents, nodesCB } from './gridstack.component';
import {
  GridstackComponent,
  GridstackModule,
  NgGridStackOptions,
  NgGridStackWidget,
  elementCB,
  gsCreateNgComponents,
  nodesCB,
} from 'gridstack/dist/angular';
import { NgIf } from '@angular/common';
import { RecentTransactionsComponent } from './recent-transactions/recent-transactions.component';
import { UserChartsComponent } from './user-charts/user-charts.component';
import { BalanceSheetModel } from '../../../../models/user-balance-sheet';
import { FinanceStatement } from '../../../../models/finance-statement';

// unique ids sets for each item for correct ngFor updating
let ids = 1;

@Component({
  selector: 'app-user-dashboard',
  standalone: true,
  imports: [
    NgIf,
    UserChartsComponent,
    GridstackModule,
    RecentTransactionsComponent,
  ],
  templateUrl: './user-dashboard.component.html',
  styleUrl: './user-dashboard.component.scss',
})
export class UserDashboardComponent {
  balanceStatement: BalanceSheetModel = new BalanceSheetModel();
  financeStatement: FinanceStatement = new FinanceStatement();

  constructor(
    public httpService: HttpService,
    private authService: AuthService
  ) {
    this.httpService
      .getAllTransactions(this.authService.getCurrentUser().userId)
      .subscribe((data) => {
        if (data) {
          this.httpService.updateExpenseBalanceSheet(data.response);
          this.httpService.userExpenseBalanceSheet.subscribe((data) => {
            this.balanceStatement = data;
          });
        }
      });

    this.httpService
      .getFinanceStatement(this.authService.getCurrentUser().userId)
      .subscribe((data) => {
        if (data) {
          this.httpService.updateFinanceStatement(data.response);
          this.httpService.userFinianceStatement.subscribe((data) => {
            this.financeStatement = data;
          });
        }
      });

  }

  ngAfterViewInit(): void {
    const grid = GridStack.init({
      cellHeight: 70,
      margin: 5,
      staticGrid: false,
      animate: true,
      // float: true,
      resizable: {
        handles: 'e, se, s, sw, w',
      },
      draggable: {
        handle: '.grid-stack-item-content',
      },
    });

    // Event listeners
    grid.on('change', (event, items) => {
      console.log('Layout changed:', items);
      // Handle layout change event
    });

    grid.on('dragstart', (event, ui) => {
      console.log('Drag started:', ui);
      // Handle drag start event
    });

    grid.on('dragstop', (event, ui) => {
      console.log('Drag stopped:', ui);
      // Handle drag stop event
    });

    grid.on('resizestart', (event, ui) => {
      console.log('Resize started:', ui);
      // Handle resize start event
    });

    grid.on('resizestop', (event, ui) => {
      console.log('Resize stopped:', ui);
      // Handle resize stop event
    });
  }

  layoutChange(event: any, id: any) {
    console.log(id + ': ' + event);
  }
}
