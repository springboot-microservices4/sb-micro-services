import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../../../shared/shared.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faAngleRight, faBell, faGears, faMagnifyingGlass, faMessage } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from '../../../../authentication/auth.service';
import { RouterLink, RouterLinkActive } from '@angular/router';
import { HttpService } from '../../../../shared/http.service';
@Component({
  selector: 'app-user-header',
  standalone: true,
  imports: [RouterLink, RouterLinkActive, FontAwesomeModule],
  templateUrl: './user-header.component.html',
  styleUrl: './user-header.component.scss'
})
export class UserHeaderComponent implements OnInit{
  faMessage= faMessage;
  faAlert = faBell;
  angleRight = faAngleRight;
  admin = faGears;
  search = faMagnifyingGlass;
  constructor(public sharedService: SharedService, private authService: AuthService, private httpService: HttpService) {

  }

  ngOnInit(): void {
    this.httpService
    .getCategories(this.authService.getCurrentUser().userId)
    .subscribe((data) => {
      if (data) {
        this.httpService.updateCategories(data.response);
      }
    });

    this.httpService.getPaymentTypes(this.authService.getCurrentUser().userId).subscribe(data => {
      if(data) {
        this.httpService.updatePaymentTypes(data.response);
      }
    });


    this.httpService.getCardDetailsList(this.authService.getCurrentUser().userId).subscribe(data => {
      if(data) {
        this.httpService.updateCardDetailsList(data.response);
      }
    })
  }

  logout() {
    this.authService.logout();
  }

  
  
}
