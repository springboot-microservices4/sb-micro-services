import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { NgClass } from '@angular/common';
import { SharedService } from '../../../../shared/shared.service';
import { UserFooterComponent } from '../user-footer/user-footer.component';
import { UserHeaderComponent } from '../user-header/user-header.component';
import { UserSideComponent } from '../user-side/user-side.component';

@Component({
  selector: 'app-user-layout',
  standalone: true,
  imports: [RouterOutlet, UserHeaderComponent, UserFooterComponent, UserSideComponent, NgClass],
  templateUrl: './user-layout.component.html',
  styleUrl: './user-layout.component.scss'
})
export class UserLayoutComponent {

  constructor(public sharedService: SharedService) {

  }
}
