import { NgClass } from '@angular/common';
import { Component } from '@angular/core';
import { RouterLink, RouterLinkActive } from '@angular/router';
import { SharedService } from '../../../../shared/shared.service';
import { AuthService } from '../../../../authentication/auth.service';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faSquare, faCheckSquare, faHouse, faCoffee, faFileInvoice, faSignOut, faCoins, faMoneyBillTransfer, faCommentDollar, faCommentsDollar, faCalculator } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-user-side',
  standalone: true,
  imports: [NgClass, RouterLink, RouterLinkActive,FontAwesomeModule],
  templateUrl: './user-side.component.html',
  styleUrl: './user-side.component.scss'
})
export class UserSideComponent {
  faCoffee = faCoffee;
  faHouse = faHouse;
  signOut = faSignOut;
  coins = faCoins;
  invoice = faFileInvoice;
  moneyTransactions = faMoneyBillTransfer;
  finance = faCommentsDollar;
  calculator = faCalculator;
  constructor(public sharedService: SharedService, private authService: AuthService
  ) {
  }

  logout() {
    this.authService.logout();
  }
}
