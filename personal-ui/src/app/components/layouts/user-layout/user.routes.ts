import { Routes } from '@angular/router';
import { InwardsComponent } from './inwards/inwards.component';
import { OutwardsComponent } from './outwards/outwards.component';
import { UserDashboardComponent } from './user-dashboard/user-dashboard.component';

export const userRoutes: Routes = [
    {path:'', redirectTo: 'dashboard', pathMatch:'full'},
    {
        path:'dashboard', 
        component: UserDashboardComponent
    },
    {
        path:'inwards', 
        component: InwardsComponent
    },
    {
        path:'outwards', 
        component: OutwardsComponent
    }
];
