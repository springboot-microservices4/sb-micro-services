export class BsModel {
  amtType?: string;
  balance?: number;
  createdDate?: string;
  inwardAmt?: number;
  modifiedDate?: string;
  outwardAmt?: number;
  statementId?: any;
  userId?: any;
  description?: any;
  transactionDate?: string;
  category: any;
  paymentType: any;
  cardDetails: any;
}
