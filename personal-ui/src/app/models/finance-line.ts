export class FinacneLine {
  borrowerName?: any;
  createdDate?: any;
  days?: any;
  financeType?: any;
  interest?: any;
  interestRate?: any;
  lendOrBorrowDate?: any;
  lenderName?: any;
  modifiedDate?: any;
  months?: any;
  principleAmt?: any;
  status?: any;
  totalAmount?: any;
  userId?: any;
  years?: any;
}
