import { FinacneLine } from "./finance-line";

export class FinanceStatement {
  financeLines?: FinacneLine[];
  totalAmount?: number;
  totalInterest?: number;
  totalPriciple?: number;
  userId?: any;
}
