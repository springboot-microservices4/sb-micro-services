import { BsModel } from './bs-model';

export class BalanceSheetModel {
  bsModel?: BsModel[];
  finalBalance?: number;
  totalInwards?: number;
  totalOutWards?: number;
  category?: any;
	cardDetails?: any;
	paymentType?: any;
  userId?: 1;
}
