export class User {
    userId?: number;
    userEmail?: string;
    password?: string;
    firstName?: string;
    lastName?: string;
    token?: string;
    isUserLoggedIn:boolean = false;
    mobileNo?: string;
}