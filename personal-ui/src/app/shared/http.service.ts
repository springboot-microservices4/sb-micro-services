import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { GET_BALANCE_STATEMENT, GET_CARD_DETAILS_LIST, GET_CATEGORIES, GET_FINANCE_STATEMENT, GET_LOGGED_IN_USER, GET_PAYMENT_TYPE_LIST, SAVE_CARD_DETAILS, SAVE_CATEGORY, SAVE_PAYMENT_TYPE, SAVE_TRANSACTION } from './shared.consts';
import { BalanceSheetModel } from '../models/user-balance-sheet';
import { FinanceStatement } from '../models/finance-statement';
import moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  balanceSheetBS = new BehaviorSubject<BalanceSheetModel>({});
  financeStatementBS = new BehaviorSubject<FinanceStatement>({});
  categoriesBs = new BehaviorSubject<any>({});
  paymentTypesBs = new BehaviorSubject<any>({});
  cardDetailsListBs = new BehaviorSubject<any>({});

  // expose the BehaviorSubject as an Observable
  userExpenseBalanceSheet = this.balanceSheetBS.asObservable();
  userFinianceStatement = this.financeStatementBS.asObservable();
  categoriesBsList = this.categoriesBs.asObservable();
  paymentTypesBsList = this.paymentTypesBs.asObservable();
  cardDetailsListBsList = this.cardDetailsListBs.asObservable();

  // function to update the value of the BehaviorSubject
  updateExpenseBalanceSheet(newSheet: BalanceSheetModel){
    this.balanceSheetBS.next(newSheet);
  }

  updateFinanceStatement(newStatement: FinanceStatement){
    this.financeStatementBS.next(newStatement);
  }

  updateCategories(newStatement: any){
    this.categoriesBs.next(newStatement);
  }

  updatePaymentTypes(newStatement: any){
    this.paymentTypesBs.next(newStatement);
  }
  
  updateCardDetailsList(newStatement: any){
    this.cardDetailsListBs.next(newStatement);
  }

  constructor(
    private http: HttpClient,
  ) { }

  getCurrentUser(): Observable<any> {
    return this.http.get<any>(`${environment.apiServerURl}${GET_LOGGED_IN_USER}`);
  }

  saveTransaction(trForm: any): Observable<any> {
    return this.http.post<any>(`${environment.apiServerURl}${SAVE_TRANSACTION}`, trForm);
  }

  getAllTransactions(userId: any): Observable<any> {
    return this.http.get<any>(`${environment.apiServerURl}${GET_BALANCE_STATEMENT}${userId}`);
  }
  getFinanceStatement(userId: any): Observable<any> {
    return this.http.get<any>(`${environment.apiServerURl}${GET_FINANCE_STATEMENT}${userId}`);
  }

  getCardDetailsList(userId: any): Observable<any> {
    return this.http.get<any>(`${environment.apiServerURl}${GET_CARD_DETAILS_LIST}${userId}`);
  }

  saveCardDetails(cardForm: any): Observable<any> {
    return this.http.post<any>(`${environment.apiServerURl}${SAVE_CARD_DETAILS}`, cardForm);
  }

  getCategories(userId: any): Observable<any> {
    return this.http.get<any>(`${environment.apiServerURl}${GET_CATEGORIES}${userId}`);
  }

  saveCategory(cardForm: any): Observable<any> {
    return this.http.post<any>(`${environment.apiServerURl}${SAVE_CATEGORY}`, cardForm);
  }

  getPaymentTypes(userId: any): Observable<any> {
    return this.http.get<any>(`${environment.apiServerURl}${GET_PAYMENT_TYPE_LIST}${userId}`);
  }

  savePaymentType(cardForm: any): Observable<any> {
    return this.http.post<any>(`${environment.apiServerURl}${SAVE_PAYMENT_TYPE}`, cardForm);
  }

  changeDateFormat(date: any, format: any) {
    return moment(new Date(date)).format(format);
  }
}
