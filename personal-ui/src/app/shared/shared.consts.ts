export const TOKEN_URL = "/user/api/oauth/token";
export const GET_LOGGED_IN_USER = "/user/api/get-current-user";
export const GET_BALANCE_STATEMENT = '/expense/api/v1/get/balance-statement/';
export const GET_FINANCE_STATEMENT = '/finance/api/v1/get/finance-statement/'
export const SAVE_TRANSACTION = '/expense/api/v1/post/save-transaction'
export const GET_CARD_DETAILS_LIST = '/expense/api/v1/get/card-details/';
export const SAVE_CARD_DETAILS = '/expense/api/v1/post/add-card-details'
export const GET_CATEGORIES = '/expense/api/v1/get/categories/';
export const SAVE_CATEGORY = '/expense/api/v1/post/add-category'
export const GET_PAYMENT_TYPE_LIST = '/expense/api/v1/get/payment-types/';
export const SAVE_PAYMENT_TYPE = '/expense/api/v1/post/add-payment-type'

export const  TRANSACTION_TYPES = [
    { label: "Credit", value: 'CREDIT' },
    { label: "Debit", value: 'DEBIT' },
  ];

  export const  CARD_TRANSACTION_TYPES = [
    { label: "Bill Pay", value: 'BILL_PAY' },
    { label: "EMI", value: 'EMI' },
    { label: "Shopping", value: 'SHOPPING' },
    { label: "Withdrawl", value: 'WITHDRAWL' }
  ];

  export const DATE_CONFIG = { isAnimated: true, dateInputFormat: 'DD MMM, YYYY' }