import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  isADBMenuToggle: boolean = true;
  constructor() { }
}
